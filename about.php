<?php
  session_start();
  include_once('api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>About ZerenthalRPG</title>

<?php
imports();
 ?>

</head>

<body onload="onload();">

<?php print_header(2); ?>

<div class="main" id="main">

<div class="body">

<h1>About</h1>

<?php
$thread = Thread::fromId(14, true);
echo markdown($thread->content);
 ?>

</div>

</div>

</body>

</html>
