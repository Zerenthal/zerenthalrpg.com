<?php
  session_start();
  include_once('api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>ZerenthalRPG Notifications</title>

<?php
imports();
 ?>

</head>

<body onload="onload();">

  <?php

  $user = getUser();
  $db = new db();
  if($user->id !== -1) {
    $stmt = $db->prepare("UPDATE Notifications SET Seen=1 WHERE Seen=0 AND UserId=?");
    $stmt->bind_param("i",$user->id);
    $db->exec();
  }
  print_header(-1); ?>

  <div class="main" id="main">

    <div class="body">

      <h1>Notifications</h1>
      <a href="<?php echo $user->getLink(); ?>/edit/notifications">
        <div class="btn right">Settings</div>
        <div class="clear-right"></div>
      </a>
      <ul class="notifications-page">
      <?php
      $db->prepare("SELECT ThreadId,PostId,Type,Content,`Date` FROM Notifications WHERE UserId=$user->id ORDER BY `Date` DESC");
      $db->exec();
      $result = $db->get();
      if($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          $poster = ($row['Type'] === 2 ? getUserFromId($row['PostId']) : getUserFromId(Post::fromId($row['PostId'])->userId));
          if($row['Type'] === 1 && $row['PostId'] === -1) {
            $thread = Thread::fromId($row['ThreadId'],false);
            $poster = getUserFromId($thread->userId);
          }

          $suffix = "";
          if($row['PostId'] !== -1) {
            $db2 = new db();
            $stmt2 = $db2->prepare("SELECT Id FROM Posts WHERE ThreadId=?");
            $stmt2->bind_param("i",$row['ThreadId']);
            $db2->exec();
            $result2 = $db2->get();
            $threadPage = 1;
            $i = 1;
            while($row2 = $result2->fetch_assoc()) {
              if($row2['Id'] === $row['PostId']) {
                $threadPage = ceil($i / 10);
                break;
              }
              $i++;
            }
            $suffix = "/page-$threadPage#post".$row['PostId'];
          }

          $content = markdown($row['Content']);
          $thread = Thread::fromId($row['ThreadId'],false);
          echo "<a href=\"".$thread->getLink().$suffix."\">
            <div class=\"notification\">
              <img class=\"notification-img\" src=\"".$poster->getImage(50)."\">
              <div class=\"notification-text\"><span style=\"font-weight: bold;\">".$poster->display." ".($row['Type'] === 2 ? "edited" : ($row['Type'] === 1 ? "mentioned you" : "replied")).": </span><br>$content
              <span style=\"font-weight: bold;\">(";
          print_time($row['Date']);
          echo ")</span></div>
            </div>
          </a>";
        }
      }

       ?>
      </ul>

    </div>

  </div>

</body>

</html>
