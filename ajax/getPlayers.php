<?php
require '../api.php';
$nameArr = array();
if (isset($_GET['q'])){
  $query = $_GET['q'];
  $db = new db();
  $stmt = $db->prepare("SELECT Name, Display FROM Users WHERE Name LIKE ? OR Display LIKE ? ORDER BY Name LIMIT 10");
  $que = '%' . $query . '%';
  $stmt->bind_param("ss", $que, $que);

  $db->exec();
  $result = $db->get();

  if($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      array_push($nameArr, $row);
    }
  }
}
$json = json_encode($nameArr);
echo $json;

 ?>
