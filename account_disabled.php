<?php
  session_start();
  include_once('api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>Account Disabled</title>

<?php
imports();
 ?>

</head>

<body onload="onload();">

  <?php print_header(-1); ?>

  <div class="main" id="main">

    <div class="body">

      <div class="warning">
        You account has been disabled!
      </div>

    </div>

  </div>

</body>

</html>
