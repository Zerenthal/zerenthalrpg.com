<?php
  session_start();
  include_once('api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>ZerenthalRPG News</title>

<?php
imports();
$GLOBALS['page'] = $_GET['page'];
if($page === null) {
	$GLOBALS['page'] = 1;
}
 ?>

</head>

<body onload="onload();">

  <?php print_header(0); ?>

  <div class="main" id="main">

    <div class="body">

      <h1>News</h1>
      <?php
      $db = new db();
      $stmt = $db->prepare("SELECT Id,Name,UserId,`Date`,LastEdit FROM ThreadData WHERE TopicId=1 ORDER BY `Date` DESC LIMIT 10 OFFSET ".(($page - 1) * 10));
      $db->exec();
      $result = $db->get();

      if($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          $stmt = $db->prepare("SELECT `Text` FROM ThreadContent WHERE Id=?");
          $stmt->bind_param("i",$id);
          $id = $row['Id'];
          $db->exec();
          $resultContent = $db->get();
          $rowContent = $resultContent->fetch_assoc();
          $row_user = getUserFromId($row['UserId']);
          $thread = Thread::fromId($id, false);
       ?>
          <div class="group with-img">
            <div class="group-header">
              <a href="<?php echo $row_user->getLink(); ?>">
                <img class="side-img" src="<?php echo $row_user->getImage(64); ?>">
              </a>
              <div class="rank">
                <a href="<?php echo $row_user->getLink(); ?>" class="<?php echo $row_user->priv_name; ?>_RANK">
                  <?php echo getUserFromId($row['UserId'])->display; ?>
                </a>
              </div>
              <a href="<?php echo $thread->getLink(); ?>"><?php print_time($row['Date']); ?></a>
              <a href="<?php echo $thread->getLink(); ?>"><span class="right">
                <?php
                  $stmt = $db->prepare("SELECT COUNT(Id) AS Amount FROM Posts WHERE ThreadId=?");
                  $stmt->bind_param("i",$id);
                  $db->exec();
                  $comment_amount = $db->get()->fetch_assoc()['Amount'];
                  echo $comment_amount;
                 ?> comment<?php if ($comment_amount !== 1) echo 's' ?>
              </span></a>
              <div class="clear-right"></div>
            </div>
            <div class="group-content">
              <a href="<?php echo $thread->getLink(); ?>">
                <h3 style="margin: 0;"><?php echo $row['Name']; ?></h3>
              </a>
              <hr>
              <?php echo markdown($rowContent['Text']); ?>
            </div>
          </div>
      <?php
        }
      }
      $stmt = $db->prepare("SELECT COUNT(*) AS Amount FROM ThreadData WHERE TopicId=1");
      $db->exec();
      $resultAmount = $db->get();
      $total = $resultAmount->fetch_assoc()['Amount'];
      print_pages($page, (int)ceil($total / 10), "/news");
      ?>

    </div>

  </div>

</body>

</html>
