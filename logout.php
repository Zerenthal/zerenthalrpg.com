<?php
session_start();

$redirect = $_SESSION['lastPage'];

$_SESSION = array();

if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

if (isset($_COOKIE['Session'])) {
    unset($_COOKIE['Session']);
    setcookie('Session', null, -1, '/');
}

session_destroy();
 ?>

<!DOCTYPE HTML>

<html>

<head>

<title>Logging out</title>

<?php
  echo "<script> window.location.href = \"".$redirect."\"</script>";
 ?>

</head>

<body>

</body>

</html>
