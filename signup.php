<?php
  session_start();
  $GLOBALS['force_https'] = true;
  include_once('api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>ZerenthalRPG </title>

<?php
imports();
 ?>

</head>

<body onload="onload();">

<?php print_header(-1); ?>

<div class="main" id="main">

<div class="body">

    <h1>Sign up</h1>

<?php
$name = $_POST['uname'];
$email = $_POST['email'];
$token = $_POST['token'];
$pass1 = $_POST['pass1'];
$pass2 = $_POST['pass2'];

$print_form = true;

if($name !== null && $email !== null && $token !== null && $pass1 !== null) {
  $data = array($name, "nonExistingPlayer");
  $data_string = json_encode($data);

  $ch = curl_init('https://api.mojang.com/profiles/minecraft');
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data_string))
  );

  $mcresult = curl_exec($ch);

  if($result === "[]") {
    warn("Could not find minecraft account!");
  } else {
    $json = json_decode($mcresult, true)[0];

    $uuid = $json['id'];
    $name = $json['name'];
    $db = new db();
    $stmt = $db->prepare("SELECT Uuid, Token FROM Tokens WHERE Uuid=?");
    $stmt->bind_param("s",$uuid);
    $db->exec();
    $result = $db->get();

    $s_token = $result->fetch_assoc()["Token"];

    $stmt = $db->prepare("SELECT Name FROM Users WHERE Name=?");
    $stmt->bind_param("s",$name);
    $db->exec();
    $result = $db->get();

    if($result->num_rows > 0) {
      warn("That user is already registered on the site!");
    } else {
      if($token !== $s_token) {
        warn("Token is invalid! Please get a new token from the server.");
        $stmt = $db->prepare("DELETE FROM Tokens WHERE Uuid=?");
        $stmt->bind_param("s",$uuid);
        $db->exec();
      } else {
        if($pass1 !== $pass2) {
          warn("Passwords do not match!");
        } else {
          $stmt = $db->prepare("SELECT Email FROM Users WHERE Email=?");
          $stmt->bind_param("s",$email);
          $db->exec();
          $result = $db->get();
          $email_exists = ($result->num_rows > 0);
          if($email_exists) {
            warn("There is already another account with that email.");
          } else {
            $print_form = false;
            $stmt = $db->prepare("INSERT INTO Users (Id,Name,Display,Pwd,Salt,Uuid,Email,Confirmed,Priv,Joined,LastSeen,About,Notify,Session) VALUES (?,?,?,?,?,?,?,0,1,?,?,'',0,'');");
            $stmt->bind_param("issssssss",$id,$name,$name,$pwd,$salt,$uuid,$email,$joined,$lastSeen);
            $id = getNextUserId();
            $salt = uniqid();
            $pwd = hash("sha256",$pass1.$salt);
            $joined = gmdate(DATE_ATOM);
            $lastSeen = $joined;
            $db->exec();

            $subject = 'ZerenthalRPG Confirmation';
            $message = '<html>
            <head>
              <title>ZerenthalRPG Confirmation</title>
            </head>
            <body>
              <p>Hi '.$name.'! Thank you for registering on ZerenthalRPG.com! To complete the registration click the link below.</p>

              <a style="color:#fff;text-decoration: none;" href="http://zerenthalrpg.com/confirm.php?uuid='.$uuid.'&token='.$token.'"><div style="height:30px; width: 100%; background-color: rgb(205,165,24); text-align: center; padding-top: 10px; border-radius: 3px;">Confirm</div></a>
              <br>
            </body>
            </html>';

            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=iso-8859-1';

            $headers[] = 'To: '.$name.' <'.$email.'>';
            $headers[] = 'From: ZerenthalRPG Confirmation <support@zerenthalrpg.com>';

            mail($email, $subject, $message, implode("\r\n", $headers));

            echo "Thank you for registering! We've sent an email to confirm your account!";
          }
        }
      }
    }
  }
}

if($print_form) {
  echo "<form method=\"POST\" action=".$_SERVER['PHP_SELF'].">
      <table>
        <tbody>
          <tr>
            <td>
              <label>Minecraft username</label>
            </td>
            <td>
              <input type=\"text\" name=\"uname\" placeholder=\"Steve\" pattern=\"[a-zA-Z0-9_]{2,16}\" required=\"required\" title=\"2-16 character Minecraft username\" value=\"".$_POST["uname"]."\">
            </td>
          </tr>
          <tr>
            <td>
              <label>Email (case sensitive)</label>
            </td>
            <td>
              <input type=\"email\" name=\"email\" placeholder=\"email@example.com\" value=\"".$_POST["email"]."\">
            </td>
          </tr>
          <tr>
            <td>
              <label>Security Token (case sensitive)</label>
            </td>
            <td>
              <input type=\"text\" name=\"token\" placeholder=\"abcdef\" pattern=\"[a-z]{6}\" required=\"required\" title=\"6 character token\">
              Get on the minecraft server and type <span class=\"command\">/token</span> in chat to get your security token.
            </td>
          </tr>
          <tr>
            <td>
              <label>Password (case sensitive)</label>
            </td>
            <td>
              <input type=\"password\" name=\"pass1\" placeholder=\"password\" pattern=\".{7,}\" required=\"required\" title=\"At least 7 characters\">
              Must be at least 7 characters long.
            </td>
          </tr>
          <tr>
            <td>
              <label>Confirm password (case sensitive)</label>
            </td>
            <td>
              <input type=\"password\" name=\"pass2\" placeholder=\"password\" pattern=\".{7,}\" required=\"required\" title=\"At least 7 characters\">
            </td>
          </tr>
        </tbody>
      </table>
      <input type=\"submit\" value=\"Sign up\">
    </form>";
}
 ?>
</div>

</div>

</body>

</html>
