<?php
  session_start();
  include_once('api.php');

  if ( $_SERVER['REQUEST_METHOD'] == 'POST' && isLoggedIn()) {
    $user = getUserFromId($_GET['id']);
    if(getUser()->priv < 2) {
      $user = getUser();
    }
    $user->notify = $_POST['reply'] + $_POST['mention'] + $_POST['edit'];
    $user->update();
    echo "<script>
      window.location.href = \"/notifications\";
    </script>";
  }

  $GLOBALS['sessionUser'] = getUser();

  if($_GET['id'] === null) {
    $GLOBALS['user'] = getUser();
  } else if(userExists($_GET['id'])){
    $GLOBALS['user'] = new user($_GET['id']);
  } else {
    echo "<script>
      window.location.href = \"/404\";
    </script>";
  }
?>
<!DOCTYPE HTML>

<html>

<head>

<title>ZerenthalRPG Home</title>

<?php
imports();
 ?>

</head>

<body onload="onload();">

  <?php print_header(-1); ?>

  <div class="main" id="main">

    <div class="body">

      <h1>Notification Setting</h1>
      <form method="POST" action="/notification_settings.php?id=<?php echo $user->id; ?>">
        <table class="notification-settings">
          <tr>
            <td>
              Reply to a thread you are watching
            </td>
            <td>
              <select name="reply">
                <option value="0" <?php echo ($user->notifyA[0] === 0 ? "selected" : ""); ?>>None</option>
                <option value="1" <?php echo ($user->notifyA[0] === 1 ? "selected" : ""); ?>>Website notification</option>
                <option value="2" <?php echo ($user->notifyA[0] === 2 ? "selected" : ""); ?>>Email only</option>
                <option value="3" <?php echo ($user->notifyA[0] === 3 ? "selected" : ""); ?>>Both</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              Mention in a reply or thread
            </td>
            <td>
              <select name="mention">
                <option value="0" <?php echo ($user->notifyA[1] === 0 ? "selected" : ""); ?>>None</option>
                <option value="4" <?php echo ($user->notifyA[1] === 1 ? "selected" : ""); ?>>Website notification</option>
                <option value="8" <?php echo ($user->notifyA[1] === 2 ? "selected" : ""); ?>>Email only</option>
                <option value="12" <?php echo ($user->notifyA[1] === 3 ? "selected" : ""); ?>>Both</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              Edit of a thread you are watching
            </td>
            <td>
              <select name="edit">
                <option value="0" <?php echo ($user->notifyA[2] === 0 ? "selected" : ""); ?>>None</option>
                <option value="16" <?php echo ($user->notifyA[2] === 1 ? "selected" : ""); ?>>Website notification</option>
                <option value="32" <?php echo ($user->notifyA[2] === 2 ? "selected" : ""); ?>>Email only</option>
                <option value="48" <?php echo ($user->notifyA[2] === 3 ? "selected" : ""); ?>>Both</option>
              </select>
            </td>
          </tr>
        </table>
        <input type="submit" value="Save">
      </form>
    </div>

  </div>

</body>

</html>
