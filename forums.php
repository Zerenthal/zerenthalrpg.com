<?php
  session_start();
  include_once('api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>ZerenthalRPG Forums</title>

<?php
imports();
 ?>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">

      <?php
      $GLOBALS['user'] = getUser();
      if(isLoggedIn()) {
        $perm = $user->priv;
      } else {
        $perm = 1;
      }

      $db = new db();
      $stmt = $db->prepare("SELECT Id,Name FROM Categories WHERE Visible<=? ORDER BY Id ASC");
      $stmt->bind_param("i",$user->priv);

      $db->exec();
      $result = $db->get();

      if($perm >= 3) {
        echo "<div class=\"btn right\">
          <a class=\"btn\" href=\"/category/create\">Create Category</a>
        </div>
        <div class=\"clear-right\"></div>";
      }

      if($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          $category = Category::withData($row['Name'], 1);
          $category->id = $row['Id'];
          $category->exists = 1;
          echo "<h1 class=\"category-head\">$category->name";
          if($perm >= 3) {
            echo "<div class=\"right\"><a href=\"../category/edit-$category->id"."-".$category->getName()."\">Edit</a>|<a href=\"../category/delete-confirm-$category->id\">Delete</a></div>";
          }
          echo "</h1>";
            $topics = $category->getTopics($perm);

            if($perm >= 3) {
              echo "<div class=\"topic_btn btn right\">
                <a class=\"btn\" href=\"../topic/create-$category->id\">Create Topic</a>
              </div>
              <div class=\"clear-right\"></div>";
            }

            foreach ($topics as &$topic) {

              $link = "<a href=\"".$topic->getLink()."\">";

              $thread = $topic->getLatestThread($user);

              $poster = getUserFromId($thread->userId);

              $count = $topic->countThreads($user);

              $s = "s";
              if($count === 1) {
                $s = "";
              }

              echo "<div class=\"category with-img\">
                $link<img src=\"".$topic->image."\" class=\"side-img\"></a>
                <div class=\"category-info\">
                  $link".$topic->name."</a><br>
                  <span class=\"description\">".$topic->description."</span>
                  <div class=\"right\">".$count." thread$s</div>
                </div>
              </div>";
            }
        }
      }
      ?>

    </div>

  </div>

</body>

</html>
