<?php
  session_start();
  include_once('api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>ZerenthalRPG Home</title>

<?php
imports();
 ?>

</head>

<body onload="onload();">

<?php print_header($_GET["val"]); ?>

<div class="main" id="main">

<div class="body">

  <h1>Confirmed</h1>
  <?php

  $uuid = $_GET["uuid"];
  $token = $_GET["token"];

  $db = new db();
  $stmt = $db->prepare("SELECT Uuid FROM Tokens WHERE Uuid=? AND Token=?");
  $stmt->bind_param("ss", $uuid, $token);

  $db->exec();
  $result = $db->get();
  if($result->num_rows > 0) {
    echo "Thank you for registering! Your account has been confirmed!";
    $stmt = $db->prepare("UPDATE Users SET Confirmed=1 WHERE Uuid=?");
    $stmt->bind_param("s",$uuid);
    $db->exec();
  } else {
    echo "Sorry! That link isn't valid!";
  }
   ?>

</div>

</div>

</body>

</html>
