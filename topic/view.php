<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<?php
imports();

$id = $_GET['id'];
if($id === null) {
	echo "<script> window.location.href = \"/forums\"; </script>";
}
$GLOBALS['page'] = $_GET['page'];
if($page === null) {
	$GLOBALS['page'] = 1;
}
$GLOBALS['topic'] = Topic::fromId($id);
$GLOBALS['pageAmount'] = 50;

$GLOBALS['user'] = getUser();
 ?>

<title><?php echo $topic->name; ?></title>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">

      <?php
      if($topic->visible<=$user->priv) {
        echo "<span><a href=\"/forums\">Forums</a> → $topic->name</span>
        <h1>$topic->name</h1>";

        if($user->priv >= $topic->canPost) {
          if($user->id !== -1) {
            echo "<div style=\"margin: 15px 0 15px 0;\">
              <a class=\"btn\" href=\"/thread/create-".$_GET['id']."\">New Thread</a></div>";
          } else {
            echo "Please <a href=\"/login\">Log in</a> to post a thread.";
          }
        }

        if($user->priv >= 3) {
          echo "<div><div class=\"right\">
            <a class=\"btn\" href=\"edit-".$_GET['id']."-".$topic->getName()."\">Edit</a>
          </div>
          <div>
            <a class=\"btn btn_warn\" href=\"delete-confirm-".$_GET['id']."\">Delete Topic</a>
          </div>
          </div>";
        }

        $threads = $topic->getThreads($pageAmount, $pageAmount * ($page - 1),$user);

        foreach ($threads as &$thread) {
          $link = "<a href=\"".$thread->getLink()."\">";

          $post = $thread->getLatestPost($user);

          $poster = getUserFromId($thread->userId);
          $posterLink = "href=\"".$poster->getLink()."\"";
          $latestPoster = getUserFromId($post->userId);

          $postAmount = $thread->countPosts($user);

          echo "<div class=\"group with-img\">
            <div class=\"group-header\">
              <a $posterLink>
                <img src=\"/images/seen.png\" class=\"side-img\">
                <img src=\"".$poster->getImage(64)."\" class=\"side-img";
          if($user->hasSeen($thread->id)) {
            echo " seen";
          }
          echo "\">
              </a>
              <div class=\"rank\">
                <a $posterLink class=\"".$poster->priv_name."_RANK\">$poster->display</a>
              </div><a href=\"".$thread->getLink()."\"> ";
          print_time($thread->date);
          $reply_text = "replies";
          if($postAmount === 1)
            $reply_text = "reply";
          echo "</a><span class=\"right\">
                <a href=\"".$thread->getLink()."#replies\">".$postAmount." " . $reply_text . "</a>
              </span>
              <div class=\"clear-right\"></div>
            </div>
            <div class=\"group-content\">";


          if($thread->pinned === 1) {
            echo "<span class=\"btn\"><i class=\"material-icons\" style=\"font-size: 1em;\">attach_file</i>Pinned</span> ";
          }
          if($thread->locked === 1) {
            echo "<span class=\"btn btn_warn\"><i class=\"material-icons\" style=\"font-size: 1em;\">lock</i> Locked</span> ";
          }
          echo $link."$thread->name</a>";
          if($postAmount > 0) {
            echo"<div class=\"right\">
                  <div class=\"rank\">
                    <a href=\"".$latestPoster->getLink()."\" class=\"".$latestPoster->priv_name."_RANK\">$latestPoster->display</a>
                  </div>
                  <a href=\"".$thread->getLink(ceil($postAmount / 10))."#post".$post->id."\">replied</a> ";
            print_time($post->date);
            echo "</div>";
          }
          echo "<div class=\"clear\"></div>
          </div>
        </div>";
        }
        if(sizeof($threads) === 0) {
          echo "<div style=\"text-align: center;font-weight: bold;font-size: 1.5em;margin-top: 20px;\">This topic is empty! Be the first to post! <a style=\"font-size: 0.66em;\" class=\"btn\" href=\"/thread/create.php?id=" . $_GET['id'] . "\">New Thread</a></div>";
        }
        print_pages($page, (int)ceil(sizeof($threads) / $pageAmount), "/topic.php?id=$topic->id");
      } else {
        echo "<h1>I'm sorry, you do not have permission to view this topic.</h1>";
      }
      ?>
      </table>

    </div>

  </div>

</body>

</html>
