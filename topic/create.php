<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>ZerenthalRPG Home</title>

<?php
imports();
$GLOBALS['user'] = getUser();

if($user->priv >= 3 && isset($_POST['name'])) {
  $topic = Topic::withData($_GET['id'], $_POST['visible'], $_POST['canPost'], $_POST['name'], $_POST['desc'], $_POST['img'], $_POST['tVisibility']);
  $topic->save();
  echo "<script> window.location.href = \"".$topic->getLink()."\"; </script>";
}
 ?>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">

      <?php
      if($user->priv >= 3) {
        echo "<span><a href=\"../forums\">Forums</a> → Create Topic</span>
        <h1>Creating Topic</h1>
        <form method=\"POST\" action=\"create-".$_GET['id']."\">
          <table>
            <tbody>
              <tr>
                <td>
                  Name
                </td>
                <td>
                  <input type=\"text\" name=\"name\" placeholder=\"topic\">
                </td>
              </tr>
              <tr>
                <td>
                  Description
                </td>
                <td>
                  <input type=\"text\" name=\"desc\" placeholder=\"255 character description\">
                </td>
              </tr>
              <tr>
                <td>
                  Image Link (make sure it's square)
                </td>
                <td>
                  <input type=\"text\" name=\"img\" placeholder=\"http://imgur.com/asdf.png\">
                </td>
              </tr>
              <tr>
                <td>
                  Thread Visibility
                </td>
                <td>
                  <select name=\"tVisibility\">
                    <option value=\"1\">Everyone</option>
                    <option value=\"0\">Original poster and mods</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  Required permission to post
                </td>
                <td>
                  <select name=\"canPost\">
                    <option value=\"1\">User</option>
                    <option value=\"2\">Mod</option>
                    <option value=\"3\">Admin</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  Visibility (Who can see this topic)
                </td>
                <td>
                  <select name=\"visible\">
                    <option value=\"1\">Users</option>
                    <option value=\"2\">Mods</option>
                    <option value=\"3\">Admins</option>
                  </select>
                </td>
              </tr>
            </tbody>
          </table>
          <input type=\"submit\" value=\"Create\">
        </form>";
      } else {
        echo "<h1>You do not have permission to create a topic!</h1>";
      }
       ?>

    </div>

  </div>

</body>

</html>
