<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>ZerenthalRPG Home</title>

<?php
imports();

$user = getUser();
if($_SERVER['REQUEST_METHOD'] == 'POST' && $user->priv >= 3 && isset($_GET['id'])) {
  deleteTopics("Id=".$_GET['id']);
  ?>
  <script>
    window.location.href = "../forums";
  </script>
  <?php
}
 ?>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">
      <h1 style="text-align: center;">Are you sure you want to delete this topic?</h1>
      <form method="POST" action="delete.php?id=<?php echo $_GET['id']; ?>">
        <table>
          <tr>
            <td>
              <input type="submit" value="Delete" style="background-color: #f33;">
            </td>
            <td>
              <a href="<?php $topic = Topic::fromId($_GET['id']); echo $topic->getLink(); ?>"><input type="button" value="Cancel"></a>
            </td>
          </tr>
        </table>
      </form>
    </div>

  </div>

</body>

</html>
