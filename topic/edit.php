<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>ZerenthalRPG Home</title>

<?php
imports();
$GLOBALS['user'] = getUser();
$GLOBALS['topic'] = Topic::fromId($_GET['id']);

if($user->priv >= 3 && isset($_POST['name'])) {
  $topic->visible = $_POST['visible'];
  $topic->canPost = $_POST['canPost'];
  $topic->name = $_POST['name'];
  $topic->description = $_POST['desc'];
  $topic->image = $_POST['img'];
  $topic->threadVisibility = $_POST['tVisibility'];
  $topic->categoryId = $_POST['categoryId'];
  $topic->save();
  echo "<script> window.location.href = \"".$topic->getLink()."\"; </script>";
}
 ?>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">

      <?php
      if($user->priv >= 3) {
        echo "<span><a href=\"../forums\">Forums</a> → <a href=\"".$topic->getLink()."\">$topic->name</a> → Edit Topic</span>
        <h1>Creating Topic</h1>
        <form method=\"POST\" action=\"edit-".$_GET['id']."-".$topic->getName()."\">
          <table>
            <tbody>
              <tr>
                <td>
                  Name
                </td>
                <td>
                  <input type=\"text\" name=\"name\" placeholder=\"topic\" value=\"$topic->name\">
                </td>
              </tr>
              <tr>
                <td>
                  Description
                </td>
                <td>
                  <input type=\"text\" name=\"desc\" placeholder=\"255 character description\" value=\"$topic->description\">
                </td>
              </tr>
              <tr>
                <td>
                  Category
                </td>
                <td>
                  <select name=\"categoryId\">";

          $db = new db();
          $stmt = $db->prepare("SELECT Id,Name FROM Categories");
          $db->exec();
          $result = $db->get();

          if($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
              echo "<option value=\"".$row['Id']."\" ".($topic->categoryId === $row['Id'] ? "selected=\"selected\"" : "").">".$row['Name']."</option>";
            }
          }

          echo "  </select>
                </td>
              </tr>
              <tr>
                <td>
                  Image Link (make sure it's square)
                </td>
                <td>
                  <input type=\"text\" name=\"img\" placeholder=\"http://imgur.com/asdf.png\" value=\"$topic->image\">
                </td>
              </tr>
              <tr>
                <td>
                  Thread Visibility
                </td>
                <td>
                  <select name=\"tVisibility\">
                    <option value=\"1\" ".($topic->threadVisibility === 1 ? "selected=\"selected\"" : "").">Everyone</option>
                    <option value=\"0\" ".($topic->threadVisibility === 0 ? "selected=\"selected\"" : "").">Original poster and mods</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  Required permission to post
                </td>
                <td>
                  <select name=\"canPost\">
                    <option value=\"1\" ".($topic->canPost === 1 ? "selected=\"selected\"" : "").">User</option>
                    <option value=\"2\" ".($topic->canPost === 2 ? "selected=\"selected\"" : "").">Mod</option>
                    <option value=\"3\" ".($topic->canPost === 3 ? "selected=\"selected\"" : "").">Admin</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  Visibility (Who can see this topic)
                </td>
                <td>
                  <select name=\"visible\">
                    <option value=\"1\" ".($topic->visible === 1 ? "selected=\"selected\"" : "").">Users</option>
                    <option value=\"2\" ".($topic->visible === 2 ? "selected=\"selected\"" : "").">Mods</option>
                    <option value=\"3\" ".($topic->visible === 3 ? "selected=\"selected\"" : "").">Admins</option>
                  </select>
                </td>
              </tr>
            </tbody>
          </table>
          <input type=\"submit\" value=\"Edit\">
        </form>";
      } else {
        echo "<h1>You do not have permission to create a topic!</h1>";
      }
       ?>

    </div>

  </div>

</body>

</html>
