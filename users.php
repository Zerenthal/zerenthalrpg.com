<?php
  session_start();
  include_once('api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>ZerenthalRPG Users</title>

<?php
imports();
 ?>

</head>

<body onload="onload();">

  <?php print_header(3); ?>

  <div class="main" id="main">

    <div class="body">

      <h1>Users (<?php
        $db = new db();
        $db->prepare("SELECT COUNT(*) AS Amount FROM Users");
        $db->exec();
        $result = $db->get();
        echo ($result->fetch_assoc()['Amount']);
      ?>)</h1>

      <form method="GET" action="users.php">
        <input type="text" placeholder="Search" class="search" name="q" style="width: 200px; max-width: 100%;" value="<?php echo $_GET['q']; ?>">
      </form>

      <?php

      $users_per_page = 50;
      $amount = getNextUserId() - 1;

      $db = new db();
      $page = $_GET['page'];
      if($page === null) {
        $page = 1;
      }
      $page = (int)$page - 1;

      $start = $page * $users_per_page;

      $stmt = $db->prepare("SELECT Id,Uuid,Name,Display,Priv FROM Users WHERE Name LIKE ? OR Display LIKE ? ORDER BY Priv DESC, Display ASC LIMIT $users_per_page OFFSET $start");
      $stmt->bind_param("ss",$q,$q);
      $q = "%".$_GET['q']."%";
      $db->exec();

      $result = $db->get();

      echo "<div class=\"user-list\">";

      if($_GET['q'] === "givemearimjobandbitemyear") {
        ?>
        <h2>You've been playing for 1337 hours. #iZenith</h2>
        <?php
      }

      if($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          $row_user = new user($row['Id']);
          $user_link = $row_user->getLink();
          $priv_names = array('USER', 'MOD', 'ADMIN');
          echo "<div class=\"listuser\">
              <a href=\"$user_link\">
                <img class=\"user-avatar\" src=\"https://crafatar.com/avatars/".$row['Uuid']."?size=64\">
              </a>
              <div class=\"listuserinfo\">
                <div class=\"rank ".$priv_names[$row['Priv'] - 1]."_RANK\">
                  <a class=\"".$priv_names[$row['Priv'] - 1]."_RANK\" style=\"padding: 0;\" href=\"$user_link\">
                    ".$row['Display']."<br>
                  </a>
                </div><br>
                <i>".$row['Name']."</i>
              </div>
            </div>";
        }
      }
      echo "</div><hr>";

      print_pages($page + 1, (int)ceil($amount / $users_per_page), "/users");
       ?>

    </div>

  </div>

</body>

</html>
