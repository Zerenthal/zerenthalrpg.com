<?php
  session_start();
  include_once('api.php');

  $user = getUser();
  if($user->id !== -1) {
    $db = new db();
    $stmt = $db->prepare("UPDATE Notifications SET Seen=1 WHERE Seen=0 AND UserId=?");
    $stmt->bind_param("i",$user->id);
    $db->exec();
  }
 ?>
