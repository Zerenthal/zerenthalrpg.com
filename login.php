<?php
  session_start();
  $GLOBALS['force_https'] = true;
  include_once('api.php');

  if ($_SESSION["id"] === null) {
    $db = new db();

    $name = $_POST["email"];

    $stmt = $db->prepare("SELECT Salt FROM Users WHERE (Email=? OR Name=?)");

    $stmt->bind_param("ss", $name, $name);
    $db->exec();
    $result = $db->get();
    $salt = $result->fetch_assoc()['Salt'];

    $query = "SELECT Name, Id, Session FROM Users WHERE (Email=? OR Name=?) AND Pwd=?;";

    $stmt = $db->prepare($query);

    $stmt->bind_param("sss", $name, $name, $pass);

    $pass = hash("sha256", $_POST["pass"].$salt);

    $db->exec();
    $result = $db->get();

    if($result->num_rows > 0) {
      $row = $result->fetch_assoc();
      $_SESSION["id"] = $row["Id"];
      $session = $row["Session"];
      if($session === "") {
        $session = uniqid();
        $stmt = $db->prepare("UPDATE Users SET Session=? WHERE Id=?");
        $stmt->bind_param("si",$session,$row["Id"]);
        $db->exec();
      }
      setcookie("Session", $session, time()+60*60*24*5,"/");
    } else {
      $_SESSION["id"] = null;
      setcookie("Session", null);
    }
  }
?>
<!DOCTYPE HTML>

<html>

<head>

<title>ZerenthalRPG Login</title>

<?php
imports();
 ?>

<?php
  if($_SESSION["id"] !== null) {
    echo "<script> window.location.href = \"".$_SESSION['lastPage']."\"</script>";
  }
 ?>

</head>

<body onload="onload()">

<?php print_header(-1); ?>

<div class="main" id="main">

<div class="body">

<h1>Sign in</h1>

<?php

if($_POST["email"] !== null && $_SESSION["id"] === null) {
  warn("Incorrect username or password!");
}

 ?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Email or Username:<br>
  <input type="text" name="email" placeholder="email@example.com" required="required" <?php echo ($_POST["email"] !== null ? "value=\"" . $_POST["email"] . "\"" : ""); ?>><br>
  Password:<br>
  <input type="password" name="pass" placeholder="password" required="required"><br>
  <input type="submit">
</form>

</div>

</div>

</body>

</html>
