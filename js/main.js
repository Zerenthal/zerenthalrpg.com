var playerJson = "";

var sections = ["Home", "Forums", "About", "Test"];
var pages = ["../?val=home", "../?val=forums", "../?val=about", "../?val=test"];

var tiles = null;

var mobileMenuOpen = false;
var notificationsOpen = false;
var openedNots = false;

var lastSearch = new Date();
lastSearch.setMilliseconds(lastSearch.getMilliseconds() - searchDelay);
var lastSearchTerm;
var searchDelay = 1000;
var searchQueued = false;

var currentText = "";

var playerCache = {};

function onload() {
  updatePlayers();
  setInterval(updatePlayers,5000);
  setTimeout(removeWarnings,5000);

  pres = document.getElementsByTagName("pre");
  for(i = 0; i < pres.length; i++) {
    pre = pres[i];
    codes = pre.getElementsByTagName("code");
    for(j = 0; j < codes.length; j++) {
      code = codes[j];
      if(code.className.startsWith("language-")) {
        code.className = code.className.substring(9,code.className.length);
        pre['lang'] = code.className;
      } else {
        pre['lang'] = "unknown language";
      }
      hljs.highlightBlock(code);
    }
  }

  Ago();

  previewBtns = document.getElementsByClassName("md_preview_btn",true);
  for(i = 0; i < previewBtns.length; i++) {
    btn = previewBtns[i];
    btn.addEventListener("click", showPreview , false);
  }

  checkNameBtns = document.getElementsByClassName("check_name_btn",true);
  for(i = 0; i < checkNameBtns.length; i++) {
    btn = checkNameBtns[i];
    btn.addEventListener("click", checkDisplayName, true);
  }

  $("#m-header").bind("click",toggleMobileMenu);
  $("#main").bind("click",function(e) {
    if(mobileMenuOpen) {
      toggleMobileMenu(e);
      e.preventDefault();
    }
  });
  $(window).scroll(function(e) {
    if(mobileMenuOpen) {
      toggleMobileMenu(e);
    }
  });
  $("#notification-button").bind("click",toggleNotificationMenu);
  $("#notifications").bind("click",toggleNotificationMenu);
  $(document).bind("click",function(e) {
    if(notificationsOpen && !isDescendant(document.getElementById("notification-menu"),e.target) && e.target.id != "notification-button" && e.target.id != "notifications") {
      toggleNotificationMenu();
    }
  });
  $("#watch_btn").bind("click",function(e) {
    var xhttp = new XMLHttpRequest();
    var btn = $("#watch_btn");
    if(btn.text() === "Unwatch") {
      xhttp.open("POST","/thread/unwatch.php?id="+btn.data("thread"),true);
      btn.text("Watch");
    } else {
      xhttp.open("POST","/thread/watch.php?id="+btn.data("thread"),true);
      btn.text("Unwatch");
    }
    xhttp.send();
  });
}

function replyTo(user, content) {
  $('html, body').animate({
        scrollTop: $("#mkd_editor").offset().top
  }, 1000);
  var mkd_editor = $("#mkd_editor");
  mkd_editor.val(mkd_editor.val() + ">" + content + "\n\n@" + user);
}

function isDescendant(parent, child) {
     var node = child.parentNode;
     while (node != null) {
         if (node == parent) {
             return true;
         }
         node = node.parentNode;
     }
     return false;
}

function checkDisplayName() {
  var name = document.getElementById("text_display_name").value;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      element = document.getElementById("display_name_check");
      if(this.responseText == "true") {
        element.innerHTML = "Available!";
        element.className = "green";
      } else {
        element.innerHTML = "Not Available!";
        element.className = "red";
      }
    }
  };
  xhttp.open("POST","../api.php",true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("d=" + name);
}

function showPreview(event) {
  edit = false;
  element = event.srcElement;
  if(element.innerHTML == "Preview") {
    element.innerHTML = "Edit";
  } else {
    edit = true;
    element.innerHTML = "Preview";
  }

  console.log("Switching to " + (edit ? "edit" : "preview") + " mode.");

  element = element.parentElement;
  textarea = element.getElementsByClassName("md_textarea")[0];
  textarea.style = (edit ? "display: block;" : "display: none;");
  preview = element.getElementsByClassName("md_preview")[0];
  preview.style = (edit ? "display: none;" : "display: block;");

  preview.innerHTML = "Loading...";
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      preview.innerHTML = this.responseText;
    }
  };
  xhttp.open("POST","../markdown.php",true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  val = textarea.value;
  val = encodeURIComponent(val);
  xhttp.send("content=" + val);
}

function goto(page) {
  window.location.href = pages[page];
}

function updatePlayers() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      playerJson = this.responseText;
      parseJson();
    }
  };
  xhttp.open("GET", "http://play.zerenthalrpg.com:8000/players", true);
  xhttp.send();
}

function parseJson() {
  var output = "";
  var json = JSON.parse(playerJson);

  var max = 5;

  for (var i = 0; i < json.length && i < max; i++) {
    var player = json[i];
    output += player["name"] + "<br>";
  }
  if(json.length > max) {
    output += "and " + (json.length - max) + " more";
  }
  document.getElementById("player_amount").innerHTML = json.length;
  if(json.length == 1) {
    document.getElementById("players_are_is").innerHTML = "is";
    document.getElementById("players_plural").innerHTML = "";
  } else {
    document.getElementById("players_are_is").innerHTML = "are";
    document.getElementById("players_plural").innerHTML = "s";
  }
}

function removeWarnings() {
  Array.prototype.forEach.call(document.getElementsByClassName("fadeout"), function(e) {
    e.style = "opacity: 0;";
    setTimeout(function() {
      e.style = "display: none;";
    }, 2000);
  });
}

function toggleMobileMenu() {
  if(mobileMenuOpen) {
    var style = {"left":"","right":""};
  } else {
    var style = {"left":"-70%","right":"70%"};
  }
  $("#main").css(style);
  $("#m-header").css(style);
  mobileMenuOpen = !mobileMenuOpen;
}

function toggleNotificationMenu() {
  if(notificationsOpen) {
    $(".notification-menu").css({"top":"-300px"});
  } else {
    $(".notification-menu").css({"top":''});
    $(".notifications").css({"display":"none"});
    if(!openedNots) {
      xhttp = new XMLHttpRequest();
      xhttp.open("POST","/seen_notifications.php",true);
      xhttp.send();
      openedNots = true;
    }
  }
  notificationsOpen = !notificationsOpen;
}

function Ago(a,b){a instanceof Array||(b=a,a=void 0),a=a||document.querySelectorAll("time"),b=b||{};var c={interval:1e4,units:[["minute",60],["hour",3600],["day",86400],["week",604800],["month",2592e3],["year",31536e3]],date:function(a){return new Date(a.getAttribute("datetime"))},format:function(a,b){if(!b)return"just now";var c=0>a?" ahead":" ago";return Math.abs(a)+" "+b+c},plural:{minute:"minutes",hour:"hours",day:"days",week:"weeks",month:"months",year:"years"}};for(var d in c)b[d]=b[d]||c[d];var e=function(a){var c=b.date(a),d=((new Date).getTime()-c.getTime())/1e3,e=Math.floor(Math.abs(d)),f=null,g=null;for(var h in b.units){var i=b.units[h][1];if(!(e>=i))break;f=b.units[h][0],g=i}null!==g&&(e=Math.floor(e/g),1!=e&&(f=b.plural[f])),a.textContent=b.format(0>d?-e:e,f)},f=function(){for(var b=0;b<a.length;b++)e(a[b])};f(),setInterval(function(){f()},b.interval)}
