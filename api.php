<?php
  require('sql.php');

  setcookie("Session",'',-1,"/category");
  setcookie("Session",'',-1,"/thread");
  setcookie("Session",'',-1,"/topic");

  if($_SERVER["HTTPS"] == "on" && !$GLOBALS['force_https']) {
    header("Location: http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
  } else if($_SERVER["HTTPS"] != "on" && $GLOBALS['force_https']) {
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
  }

  $currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
  $_SESSION['lastPage'] = $_SESSION['currentPage'];
  if(strpos($currentPage,'/login') === false) {
    $_SESSION['currentPage'] = $currentPage;
  }

  if($_COOKIE['Session'] !== null) {
    if($_SESSION['id'] === null) {
      $db = new db();
      $stmt = $db->prepare("SELECT Id FROM Users WHERE Session=?");
      $stmt->bind_param("s",$_COOKIE['Session']);
      $db->exec();
      $result = $db->get();
      if($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $_SESSION['id'] = $row['Id'];
        updateUserSeen($_SESSION['id']);
      } else {
        setcookie("Session",null);
      }
    }
    setcookie("Session",$_COOKIE['Session'],time()+60*60*24*5,"/");
  }

  if($_SESSION['id'] !== null) {
    $db = new db();
    $stmt = $db->prepare("SELECT * FROM Disabled WHERE Id=?");
    $stmt->bind_param("i",$_SESSION['id']);
    $db->exec();
    if(strpos($_SERVER['REQUEST_URI'],'account_disabled') === false) {
      if($db->get()->num_rows > 0) {
        echo "<script> window.location.href = \"/disabled\"; </script>";
      }
    } else {
      if($db->get()->num_rows > 0) {
        echo "<script> window.location.href = \"/home\"; </script>";
      }
    }
  }

  if($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['d'] !== null) {
    session_start();
    $db = new db();
    $stmt = $db->prepare("SELECT Display FROM Users WHERE Display=?");
    $stmt->bind_param("s",$_POST['d']);
    $db->exec();
    $result = $db->get();

    $user = getUser();

    if($user->display === $_POST['d'] || $result->num_rows === 0){
      echo "true";
    } else {
      echo "false";
    }
  }

  include_once('Parsedown.php');

  function markdown($text) {
    $pd = new Parsedown();
    $pd->setMarkupEscaped(true);
    $pd->setBreaksEnabled(true);
    $text = $pd->text($text);
    preg_match_all('/(>|^|\s)@([^!"§$%&\/()=?.,;+*@\s]{1,16})<?/', $text, $matches, PREG_OFFSET_CAPTURE);
    $loop = $matches[0];
    for ($i = sizeof($loop) - 1; $i >= 0; $i--) {
      $playerData = $loop[$i];
      $name = substr($playerData[0],strpos($playerData[0],"@") + 1);
      if(strpos($name, "<")) {
        $name = substr($name, 0, -1);
      }
      $user = getUserFromName($name);
      if($user !== null) {
        $text = substr_replace($text, (strpos($playerData[0],">") === 0 ? ">" : "")." <span class=\"rank\" style=\"margin-bottom: 2px;\">
          <a href=\"".$user->getLink()."\" class=\"".$user->priv_name."_RANK\">@".$user->name."</a>
        </span>".(strpos($playerData[0],"<") ? "<" : ""), $playerData[1], strlen($playerData[0]));
      }
    }
    return $text;
  }

  function print_header($page) {
    $db = new db();
    $user = getUser();
    $db->prepare("SELECT COUNT(*) AS Amount FROM Notifications WHERE UserId=$user->id");
    $db->exec();
    $notificationAmount = $db->get()->fetch_assoc()['Amount'];

    echo "<div class=\"notification-menu\" id=\"notification-menu\" style=\"top: -300px;\">
    <a href=\"".$user->getLink()."/edit/notifications\">
      <div class='btn' style='display:inline-block; position: absolute; padding: 2px; z-index: 90; top: 5px; right: 5px; height: 20px;'>
        <img src='/images/gear.png' height='20px'>
      </div>
    </a>";
    $db->prepare("SELECT ThreadId,PostId,Type,Content,`Date` FROM Notifications WHERE UserId=$user->id ORDER BY `Date` DESC LIMIT 4");
    $db->exec();
    $result = $db->get();
    if($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $poster = ($row['Type'] === 2 ? getUserFromId($row['PostId']) : getUserFromId(Post::fromId($row['PostId'])->userId));
        if($row['Type'] === 1 && $row['PostId'] === -1) {
          $thread = Thread::fromId($row['ThreadId'],false);
          $poster = getUserFromId($thread->userId);
        }

        $suffix = "";
        if($row['PostId'] !== -1) {
          $db2 = new db();
          $stmt2 = $db2->prepare("SELECT Id FROM Posts WHERE ThreadId=?");
          $stmt2->bind_param("i",$row['ThreadId']);
          $db2->exec();
          $result2 = $db2->get();
          $threadPage = 1;
          $i = 1;
          while($row2 = $result2->fetch_assoc()) {
            if($row2['Id'] === $row['PostId']) {
              $threadPage = ceil($i / 10);
              break;
            }
            $i++;
          }
          $suffix = "/page-$threadPage#post".$row['PostId'];
        }

        $content = $row['Content'];
        if(strlen($content) > 40) {
          $content = substr($content, 0, 40)."...";
        }
        $row_thread = Thread::fromId($row['ThreadId'],false);
        echo "<a href=\"".$row_thread->getLink().$suffix."\">
          <div class=\"notification\">
            <img class=\"notification-img\" src=\"".$poster->getImage(40)."\">
            <div class=\"notification-text\"><span style=\"font-weight: bold;\">".$poster->display." ".($row['Type'] === 2 ? "edited" : ($row['Type'] === 1 ? "mentioned you" : "replied")).": </span><br>\"".$content."\"
            <br>
            <span style=\"font-weight: bold;\">(";
        print_time($row['Date']);
        echo ")</span></div>
          </div>
        </a>";
      }
    }

    if($notificationAmount === 0) {
      echo "<div class=\"no-notifications\">
          You have no notifications!
        </div>";
    } else {
      echo "<a href=\"/notifications\">
          <div class=\"notification-more\">View all</div>
        </a>";
    }
    echo "</div>;
    <div class='header'>";
    echo "<a href='/'><img class='logo' src='/images/icon.png'></a>";
    echo "<ul>";

    $menu_list = "";

    $db->prepare("SELECT * FROM Header");

    $db->exec();
    $result = $db->get();

    $i = 0;
    if($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $menu_list =  "$menu_list<a href='".$row["Link"]."'><li ";
        if($i === (int)$page) {
          $menu_list = $menu_list."class='active'";
        }
        $i++;
        $menu_list = $menu_list.">".$row["Name"]."</li></a>";
      }
    }

    echo "$menu_list</ul>";


    $stmt = $db->prepare("SELECT Count(*) AS Amount FROM Notifications WHERE Seen=0 AND UserId=?");
    $stmt->bind_param("i",$user->id);
    $db->exec();
    $result = $db->get();
    $row = $result->fetch_assoc();
    if ($row['Amount'] > 0) {
      $notDiv = "<div class=\"notifications\" id=\"notifications\">".$row['Amount']."</div>";
    } else {
      $notDiv = "";
    }

    if(!isLoggedIn()) {
      echo "<div class='signin'><a href='/login'>Log in</a> | <a href='/signup'>Sign up</a></div>";
    } else {
      echo "<a href='".$user->getLink()."'><img class='user-img' src=\"".$user->getImage(40)."\"></a>
      <img id=\"notification-button\" style=\"float: right; height: 40px; margin-top: 5px; cursor: pointer;\" src=\"/images/notifications.png\">
      $notDiv
      <div class='user'><span><a href='".$user->getLink()."'>".$user->name."</a><hr><a href='/logout'>Logout</a></span></div>";
    }
    ?>

    <div class="search-site">
        <form method="get" action="/search.php">
            <input type="text" id="search-site" name="q" placeholder="Enter search"/>

            <input type="submit" class="material-icons search-icon" value="search"/>
        </form>
    </div>


    <?php
    echo "</div>
    <div id=\"tiles\">";

    createTile("Players", "There <span id=\"players_are_is\">are</span> currently <code style=\"display: inline-block; background-color: #cda518; vertical-align: middle;\"><span style=\"font-weight: bold; color: #fff; font-size: 1.2em;\" id=\"player_amount\">0</span></code> player<span id=\"players_plural\">s</span> online at <code style=\"display: inline-block; background-color: #cda518; color: #fff; font-weight: bold; vertical-align: middle;\">play.zerenthalrpg.com</code>", "");

    $priv_names = array("USER","MOD","ADMIN");

    $recentUsers = "";

    $stmt = $db->prepare("SELECT Display,Id,Priv,LastSeen FROM Users ORDER BY LastSeen DESC LIMIT 5 OFFSET 0");
    $db->exec();
    $result = $db->get();

    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $date = strtotime($row['LastSeen']);
        $time = time();
        if($time - $date < 300) {
          $row_user = new user($row['Id']);
          $recentUsers = $recentUsers."<div class=\"rank\" style=\"margin-bottom: 2px;\">
            <a href=\"".$row_user->getLink()."\" class=\"".$priv_names[$row['Priv'] - 1]."_RANK\">".$row['Display']."</a>
          </div><br>";
        }
      }
    }
    createTile("Users seen in the last 5 minutes.",$recentUsers);

    $recentStaff = "";

    $stmt = $db->prepare("SELECT Display,Id,Priv,LastSeen
      FROM Users WHERE Priv>1
      ORDER BY LastSeen DESC
      LIMIT 5 OFFSET 0");
    $db->exec();
    $result = $db->get();

    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $date = strtotime($row['LastSeen']);
        $time = time();
        if($time - $date < 300) {
          $row_user = new user($row['Id']);
          $recentStaff = $recentStaff."<div class=\"rank\" style=\"margin-bottom: 2px;\">
            <a href=\"".$row_user->getLink()."\" class=\"".$priv_names[$row['Priv'] - 1]."_RANK\">".$row['Display']."</a>
          </div><br>";
        }
      }
    }
    createTile("<div class=\"rank ADMIN_RANK\">Staff</div> seen in the last 5 minutes.",$recentStaff);

    echo "</div>

    <div class=\"m-header\" id=\"m-header\">
      <img src=\"/images/menu.png\" class=\"menu-icon\" id=\"menu-icon\">
    </div>

    <div class=\"menu\" id=\"menu\">
      <ul>";

    if(isLoggedIn()) {
      echo "<a href='/account'>
          <li>
            <img src=\"".$user->getImage(25)."\"> <span>$user->display</span>
          </li>
        </a>";
    }

    if(isLoggedIn()) {
      $stmt = $db->prepare("SELECT Count(*) AS Amount FROM Notifications WHERE Seen=0 AND UserId=?");
      $stmt->bind_param("i",$user->id);
      $db->exec();
      $result = $db->get();
      $row = $result->fetch_assoc();
      echo "<a href='/notifications'>
        <li style=\"border: none;\">
          <img src='/images/notifications.png' height='25px' style='border: none;'>
          <span>
            Notifications ";
      if($row['Amount'] > 0) {
        echo "<div class='notifications' style='position:initial; padding: 0 6px 0 4px;'>".$row['Amount']."</div>";
      }
      echo "</span>
        </li>
      </a>";
    }
    echo "<div class=\"browse\">Browse</div>
      $menu_list
      </ul>".
      (isLoggedIn() ? "<a href='/logout'>
        <div class=\"m-user\">
          Logout
        </div>
      </a>
      " :
      "<a class=\"m-login\" href='/login'>
        Log in
      </a>
      <a class=\"m-signup\" href='/signup'>
        Sign up
      </a>").
    "</div>";
  }

  function search($query){
    $response = http_get("https://yandex.com/search/xml?l10n=en&user=justinfernald&key=03.513965501:cbf9ecc3d4c29c7423bbadf828fe4e56&query=$query%20host%3Azerenthalrpg.com&filter=none&sortby=rlv");
    if ($response){
      return $response;
    }
    return false;
  }

  function createTile($name, $content, $id = null) {
    echo "<div class=\"tile\"><div class=\"tile-header\">$name</div><div class=\"content\" ".($id === null ? "" : "id=$id").">$content</div></div>";
  }

  function meta($property, $content) {
    echo "<meta property=\"$property\" content=\"$content\" />";
  }

  function metaInfo($title, $url, $img) {
    meta("og:title",$title);
    meta("og:type","webpage");
    meta("og:url",$url);
    meta("og:image",$img);
  }

  function console($message) {
    echo "<script>
      console.log('$message');
    </script>";
  }

  function consoleDump($obj) {
    echo "<script>
      console.log('";
    var_dump($obj);
    echo "');</script>";
  }

  function notifyUsers($thread, $post, $type) {
    $db = $GLOBALS['db'] = new db();
    $stmt = $db->prepare("SELECT UserId FROM Watched WHERE ThreadId=?;");
    $stmt->bind_param("i",$thread->id);
    $db->exec();
    $result = $db->get();
    $usersNotified = array();
    if($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $userId = $row['UserId'];
        array_push($usersNotified, $userId);
        notify($userId,$thread,$post,$type);
      }
    }
    return $usersNotified;
  }

  function notifyMentioned($thread, $post, $notified) {
    preg_match_all('/(^|\s)@([^!"§$%&\/()=?.,;+*@\s]{1,16})/', $post->content, $matches);
    foreach ($matches[0] as $player) {
      $user = getUserFromName(substr($player,strpos($player,"@") + 1));
      if(!in_array($user->id, $notified)) {
        notify($user->id, $thread, $post, 1);
      }
    }
  }

  function notify($userId, $thread, $post, $type) {
    $poster = getUserFromId($thread->userId);
    $user = getUserFromId($userId);
    $message = getNotifyMessage($type,$post,$thread)."
    <a href=\"http://zerenthalrpg.com/thread/".$thread->id."-".$thread->getName().($post === null ? "" : "#post".$post->id)."\">
    <h2 style=\"color: #cda518; text-decoration: none; margin: 10px 0;\">".$thread->name."</h2>
    </a><div style=\"margin-left: 15px; font-style: italic; background-color: #ddd;\">".markdown($type === 2 ? $thread->content : $post->content)."</div>";
    $notify = $user->notifyA[$type];

    // 0 = no notifications
    // 1 = notification on website
    // 2 = notification by email
    // 3 = notification by both

    // 0 = Replied to thread 1:website 2:email
    // 1 = Mentioned 4:website 8:email
    // 2 = Edited thread 16:website 32:email

    if($notify === 1 || $notify === 3) {
      $db = $GLOBALS['db'];
      $stmt = $db->prepare("INSERT INTO Notifications VALUES (?,?,?,?,?,?,?,?);");
      $stmt->bind_param("iiiiissi",$id,$userId,$thread->id,$postId,$type,$date,$content,$seen);
      $id = getNextId("Notifications");
      $seen = 0;
      $postId = ($type === 2 ? $post->userId : $post->id);
      $date = gmdate(DATE_ATOM);
      $content = ($type === 2 ? $thread->name : $post->content);
      $db->exec();
      $db->close();
    }
    if($notify === 2 || $notify === 3) {
      email($userId, $message);
    }
  }

  function getNotifyMessage($type, $post, $thread) {
    if($type < 2) {
      $user = getUserFromId($post->userId);
    } else {
      $user = getUserFromId($thread->userId);
    }
    switch ($type) {
      case 0:
        return "$user->name replied to a thread you're watching:";
        break;
      case 1:
        return "$user->name mentioned you:";
        break;
      case 2:
        return "$user->name edited a thread you're watching:";
        break;
      default:
        break;
    }
  }

  function email($userId, $message) {
    $user = getUserFromId($userId);
    $subject = 'ZerenthalRPG';
    $message = '<html>
    <head>
      <title>ZerenthalRPG</title>
    </head>
    <body>
      '.$message.'
    </body>
    </html>';

    $headers[] = 'MIME-Version: 1.0';
    $headers[] = 'Content-type: text/html; charset=iso-8859-1';

    $headers[] = 'To: '.$user->display.' <'.$user->email.'>';
    $headers[] = 'From: ZerenthalRPG Forums <support@zerenthalrpg.com>';

    mail($user->email, $subject, $message, implode("\r\n", $headers));
  }

  function warn($message) {
    echo "<div class=\"warning fadeout\">$message</div>";
  }

  function info($message) {
    echo "<div class=\"info_box fadeout\">$message</div>";
  }

  function getNextUserId() {
    return getNextId("Users");
  }

  function getNextId($table) {
    $db = new db();
    $stmt = $db->prepare("SELECT Id FROM $table ORDER BY Id DESC LIMIT 1");

    $db->exec();
    $result = $db->get();

    if($result->num_rows === 0) {
      return 1;
    } else {
      return ((int) $result->fetch_assoc()["Id"]) + 1;
    }
  }

  function isLoggedIn() {
    return ($_SESSION['id'] !== null);
  }

  function updateUserSeen($id) {
    $db = new db();
    $stmt = $db->prepare("UPDATE Users SET LastSeen=? WHERE Id=?");
    $stmt->bind_param("si",$date,$id);
    $date = gmdate(DATE_ATOM);
    $db->exec();
  }

  function getUser() {
    if($_SESSION['id'] === null) {
      return new user(-1);
    } else {
      updateUserSeen($_SESSION['id']);
      return new user($_SESSION['id']);
    }
  }

  $users = array();

  function getUserFromId($id) {
    if($users[$id] !== null) {
      return $users[$id];
    } else {
      $user = new user($id);
      $users[$id] = $user;
      return $user;
    }
  }

  function getUserFromName($name) {
    $db = new db();
    $stmt = $db->prepare("SELECT Id FROM Users WHERE Name=?");
    $stmt->bind_param("s",$name);
    $db->exec();
    $result = $db->get();
    if($result->num_rows > 0) {
      $user = new user($result->fetch_assoc()['Id']);
      return $user;
    } else {
      return null;
    }
  }

  function userExists($id) {
    return ($id < getNextUserId());
  }

  function imports() {
    echo "
    <script src=\"https://code.jquery.com/jquery-3.1.1.min.js\" type=\"text/javascript\"></script>
    <script src=\"/js/textcomplete.min.js\"></script>
    <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css\">
    <script src=\"https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js\"></script>
    <script async src=\"//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.9.0/highlight.min.js\"></script>
    <script async src=\"/js/main.min.js\" type=\"text/javascript\"></script>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <link rel=\"stylesheet\" title=\"Hopscotch\" href=\"https://highlightjs.org/static/demo/styles/tomorrow-night-eighties.css\">
    <link href=\"/style/main.min.css\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"/style/mobile.min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"only screen and (max-width: 900px)\">
    <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\"
      rel=\"stylesheet\">";
  }

  function print_time($time) {
    $date = strtotime($time);
    $time_format = gmdate("l jS \of F Y h:i:s A",$date);
    echo "<time datetime='$time' title='$time_format UTC'>$time</time>";
  }

  function print_editor($name, $placeholder, $content) {
    ?>
    <div class="post-wrapper">
        <textarea id="textarea" name="<?php echo $name; ?>" rows="8" cols="80"><?php echo $content; ?></textarea>
    </div>
    <script>
    var simplemde = new SimpleMDE({
        element: document.getElementById("textarea"),
        hideIcons: ["side-by-side", "fullscreen", "guide"],
        showIcons: ["code", "table", "horizontal-rule"],
        status: false,
        spellChecker: false,
        placeholder: "<?php echo $placeholder; ?>",
        codeSyntaxHighlighting: true
    });

    var toolbar = document.getElementsByClassName("editor-toolbar")[0];
    var submitBtn = document.createElement("input");
    submitBtn.type = "submit";
    submitBtn.className = "post-submit";
    toolbar.appendChild(submitBtn);
    var clear = document.createElement("div");
    clear.style.clear = "right";
    toolbar.appendChild(clear);

    document.getElementsByClassName("CodeMirror")[0].setAttribute("contenteditable","true");

    function simplemdeAdapter(element, completer, option) {
      this.initialize(element, completer, option);
      console.trace();
    }

    function _getTextAfterCaret() {
      var cursor = simplemde.codemirror.getCursor();
      var out = "";
      out += simplemde.codemirror.getLine(cursor.line).substring(cursor.ch);
      if(cursor.line != simplemde.codemirror.lastLine()) {
        for(var i = cursor.line; i <= simplemde.codemirror.lastLine(); i++) {
          out += simplemde.codemirror.getLine(i);
        }
      }
      return out;
    }

    $.extend(simplemdeAdapter.prototype, $.fn.textcomplete.Adapter.prototype, $.fn.textcomplete.ContentEditable.prototype, {
      _bindEvents: function() {
        var $this = this;
        // this.$el.keyup(function(event) {
        //   $this._onKeyup(event);
        //   if($this.completer.dropdown.shown && $this._skipSearch(event)) {
        //     return false;
        //   }
        // },null,null,1);
        var asyncFocus = function() {
          setTimeout(function() {
            simplemde.codemirror.focus();
          },10);
        }

        $('#textcomplete-dropdown-1').mousedown(function() {
          if($this.completer.dropdown.shown) {
            asyncFocus();
          }
        });

        var blockedKeys = [27,13,38,40];
        simplemde.codemirror.on("keydown", function(codemirror, event) {
          if($this.completer.dropdown.shown && blockedKeys.includes(event.keyCode)) {
            event.preventDefault();
            asyncFocus();
          }
        });
        // simplemde.codemirror.on("blur", function(codemirror, event) {
        // });

        this.$el.on('keyup.' + this.id, $.proxy(this._onKeyup, this));
      },
      select: function(value, strategy, e) {
        var pre = this.getTextFromHeadToCaret();

        var sel = simplemde.codemirror.getSelection();

        var post = _getTextAfterCaret();
        var newSubstr = strategy.replace(value, e);
        var regExp;
        if(typeof newSubstr !== 'undefined') {
          if($.isArray(newSubstr)) {
            post = newSubstr[1] + post;
            newSubstr = newSubstr[0];
          }
          regExp = $.isFunction(strategy.match) ? strategy.match() : strategy.match;
          pre = pre.replace(regExp, newSubstr);

          simplemde.value(pre);
          var cursor = simplemde.codemirror.getCursor();
          cursor.line = simplemde.codemirror.lastLine();
          cursor.ch = simplemde.codemirror.getLine(cursor.line).length;
          simplemde.codemirror.setCursor(cursor);
          cursor = simplemde.codemirror.getCursor();
          simplemde.value(simplemde.value() + post);
          simplemde.codemirror.setCursor(cursor);
        }
      },

      // return {top: , left: , lineHeight: }
      _getCaretRelativePosition: function() {
        var editorPosition = $('.CodeMirror').position();
        var caretPosition = simplemde.codemirror.cursorCoords();
        var top = caretPosition.top - editorPosition.top;
        var left = caretPosition.left - editorPosition.left;

        var lines = simplemde.codemirror.lineAtHeight(caretPosition.top);
        var total = 0;
        for(var i = 0; i < lines; i++) {
          total += simplemde.codemirror.heightAtLine(i);
        }
        total /= lines;
        return {
          top: top,
          left: left,
          lineHeight: total
        };
      },

      getTextFromHeadToCaret: function() {
        var cursor = simplemde.codemirror.getCursor();
        var out = "";
        for(var i = 0; i < cursor.line; i++) {
          out += simplemde.codemirror.getLine(i) + "\n";
        }
        out += simplemde.codemirror.getLine(cursor.line).substring(0,cursor.ch);
        return out;
      }
    });

    $('.CodeMirror').textcomplete([{
      id: 'mkd_editor',
      match: /(\n|^|\s)@(([^!"§$%&\/()=?.,;+*@\s]{1,16} ?){0,1}[^!"§$%&\/()=?.,;+*@\s]{1,16})$/,
      search: function (str, callback) {
        currentText = str;
          if(str.length > 0) {
            if(playerCache[str]) {
              callback(playerCache[str]);
              lastSearchTerm = str;
            } else {
              var searchFunct = function(cursor, char) {
                var xmlhttp = new XMLHttpRequest();
                var playerList = [];
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        playerList = JSON.parse(this.responseText);
                        for(var i = 0; i < playerList.length; i++) {
                          if(char == "\n" || char == " ") {
                            playerList[i].prefix = char;
                          }
                        }

                        playerCache[currentText] = playerList;
                        callback(playerList);
                        searchQueued = false;
                    }
                };
                xmlhttp.open("GET", "/ajax/getPlayers.php?q=" + currentText, true);
                xmlhttp.send();
                lastSearch = new Date();
                lastSearchTerm = currentText;
                console.log("searching " + currentText);
              }
              var now = new Date();
              if(!searchQueued && now.getMilliseconds() - searchDelay > lastSearch.getMilliseconds()) {
                var cursor = simplemde.codemirror.getCursor();
                var char = simplemde.codemirror.getLine(cursor.line).substring(cursor.ch - (str.length + 2),cursor.ch - (str.length + 1));
                if(char == undefined && simplemde.codemirror.getLine(cursor.line).startsWith("@" + str) && cursor.line != 0) {
                  char = "\n"
                }
                searchFunct(cursor, char);
              } else if(!searchQueued) {
                if(playerCache[lastSearchTerm]) {
                  callback(playerCache[lastSearchTerm]);
                } else {
                  callback([]);
                }
                var cursor = simplemde.codemirror.getCursor();
                var char = simplemde.codemirror.getLine(cursor.line).substring(cursor.ch - (str.length + 2),cursor.ch - (str.length + 1));
                if(char == '' && simplemde.codemirror.getLine(cursor.line).startsWith("@" + str) && cursor.line != 0) {
                  char = "\n"
                }
                setTimeout(function() {
                  searchFunct(cursor, char)
                }, searchDelay - ((new Date()).getMilliseconds() - lastSearch.getMilliseconds()));
                searchQueued = true;
              } else {
                if(playerCache[lastSearchTerm]) {
                  callback(playerCache[lastSearchTerm]);
                } else {
                  callback([]);
                }
              }
            }
          }
      },
      template: function (object) {
        if(object.Name == object.Display) {
          return object.Name;
        } else {
          return object.Name + "(" + object.Display + ")";
        }
      },
      replace: function (object) {
        console.log(object.prefix);
        out = (object.prefix ? object.prefix : "") + '@' + object.Name + ' ';
        return out;
      }
    }], {
        adapter: simplemdeAdapter
    });
    </script>
    <?php
  }

  function print_pages($current, $total, $link_to) {
    $start = $current - 5;
    if($start < 1) {
      $start = 1;
    }
    $end = $current + 5;
    if($end > $total) {
      $end = $total;
    }

    $pref = "page-";

    if(substr($link_to, -1) !== "/") {
      $pref = "/$pref";
    }

    $pref = $link_to.$pref;

    echo "<div class=\"pages\">";

    if($current > 1) {
      echo "<div><a href=\"".$pref."1\"><< First</a></div>";
    }

    if($start > 1) {
      echo "<div>...</div>";
    }

    if($total > 1) {
      for($i = $start; $i <= $end; $i++) {
        if((int)$i === (int)$current) {
          echo "<div class=\"current\"> $i </div>";
        } else {
          echo "<div><a href=\"".$pref.$i."\"> $i </a></div>";
        }
      }
    }

    if($end < $total) {
      echo "<div>...</div>";
    }

    if($current < $total) {
      echo "<div><a href=\"".$pref.$total."\">Last >></a></div>";
    }

    echo "</div>";
  }

  function deleteCategories($where) {
    $db = new db();
    $db->prepare("SELECT Id FROM Categories WHERE $where");
    $db->exec();
    $result = $db->get();
    if($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $id = $row['Id'];
        deleteTopics("CategoryId=$id");
      }
    }
    $db->prepare("DELETE FROM Categories WHERE $where");
    $db->exec();
  }

  function deleteTopics($where) {
    $db = new db();
    $db->prepare("SELECT Id FROM Topics WHERE $where");
    $db->exec();
    $result = $db->get();
    if($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $id = $row['Id'];
        deleteThreads("TopicId=$id");
      }
    }
    $db->prepare("DELETE FROM Topics WHERE $where");
    $db->exec();
  }

  function deleteThreads($where) {
    $db = new db();
    $stmt = $db->prepare("SELECT Id FROM ThreadData WHERE $where");
    $db->exec();
    $result = $db->get();
    if($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $id = $row['Id'];
        $db->prepare("DELETE FROM ThreadData WHERE Id=$id");
        $db->exec();
        $db->prepare("DELETE FROM ThreadContent WHERE Id=$id");
        $db->exec();
        $db->prepare("DELETE FROM Posts WHERE ThreadId=$id");
        $db->exec();
        $db->prepare("DELETE FROM Watched WHERE ThreadId=$id");
        $db->exec();
        $db->prepare("DELETE FROM Notifications WHERE ThreadId=$id");
        $db->exec();
      }
    }
  }

  function URLName($text) {
    return preg_replace("([^0-9A-Za-z-]+)","",$text);
  }

  class Category {
    var $id;
    var $name;
    var $visible;
    var $exists;

    static function fromId($id) {
      $db = new db();
      $stmt = $db->prepare("SELECT * FROM Categories WHERE Id=?");

      $stmt->bind_param("i",$id);

      $db->exec();
      $result = $db->get();

      $row = $result->fetch_assoc();

      $instance = new self();

      $instance->id = $id;
      $instance->name = $row['Name'];
      $instance->visible = $row['Visible'];
      $instance->exists = true;

      return $instance;
    }

    static function withData($name, $visible) {
      $instance = new self();

      $instance->id = getNextId("Categories");
      $instance->name = $name;
      $instance->visible = $visible;

      return $instance;
    }

    function save() {
      if ($exists) {
        $db = new db();
        $stmt = $db->prepare("UPDATE Categories SET Name=?, Visible=? WHERE Id=?");
        $stmt->bind_param("sii",$this->name,$this->visible,$this->id);
        $db->exec();
      } else {
        $db = new db();
        $stmt = $db->prepare("INSERT INTO Categories VALUES (?,?,?)");
        $stmt->bind_param("isi",$this->id,$this->name,$this->visible);
        $db->exec();
        $this->exists = true;
      }
    }

    function getName() {
      return str_replace(" ","-",$this->name);
    }

    function getTopics($perm) {
      $db = new db();
      $stmt = $db->prepare("SELECT Id,Visible,CanPost,Name,Description,Image,ThreadVisibility FROM Topics WHERE CategoryId=? AND Visible<=$perm");
      $stmt->bind_param("i",$this->id);

      $db->exec();
      $result = $db->get();

      $topics = array();

      if($result->num_rows > 0) {
        $i = 0;
        while($row = $result->fetch_assoc()) {
          $topic = Topic::withData($this->id,$row['Visible'],$row['CanPost'],$row['Name'],$row['Description'],$row['Image'],$row['ThreadVisibility']);
          $topic->id = $row['Id'];
          $topic->exists = true;
          $topics[$i] = $topic;
          $i++;
        }
      }
      return $topics;
    }
  }

  class Topic {
    var $id;
    var $categoryId;
    var $visible;
    var $canPost;
    var $name;
    var $exist;
    var $description;
    var $image;
    var $threadVisibility;

    static function fromId($id) {
      $db = new db();
      $stmt = $db->prepare("SELECT * FROM Topics WHERE Id=?");
      $stmt->bind_param("i",$id);

      $db->exec();
      $result = $db->get();

      $row = $result->fetch_assoc();

      $instance = new self();

      $instance->id = $id;
      $instance->categoryId = $row['CategoryId'];
      $instance->visible = $row['Visible'];
      $instance->canPost = $row['CanPost'];
      $instance->name = $row['Name'];
      $instance->description = $row['Description'];
      $instance->image = $row['Image'];
      $instance->threadVisibility = $row['ThreadVisibility'];
      $instance->exists = true;

      return $instance;
    }

    static function withData($categoryId, $visible, $canPost, $name, $description, $image, $threadVisibility) {
      $instance = new self();

      $instance->id = getNextId("Topics");
      $instance->categoryId = $categoryId;
      $instance->visible = $visible;
      $instance->canPost = $canPost;
      $instance->name = $name;
      $instance->description = $description;
      $instance->image = $image;
      $instance->threadVisibility = $threadVisibility;

      return $instance;
    }

    function save() {
      if($this->exists) {
        $db = new db();
        $stmt = $db->prepare("UPDATE Topics SET CategoryId=?, Name=?, Visible=?, CanPost=?, Description=?, Image=?, ThreadVisibility=? WHERE Id=?");
        $stmt->bind_param("isiissii",$this->categoryId,$this->name,$this->visible,$this->canPost,$this->description,$this->image,$this->threadVisibility,$this->id);
        $db->exec();
      } else {
        $db = new db();
        $stmt = $db->prepare("INSERT INTO Topics VALUES (?,?,?,?,?,?,?,?)");
        $stmt->bind_param("iiiisssi",$this->categoryId,$this->id,$this->visible,$this->canPost,$this->name,$this->description,$this->image,$this->threadVisibility);
        $db->exec();
        $this->exists = true;
      }
    }

    function getLink() {
      return "/topic/$this->id"."-".$this->getName();
    }

    function getName() {
      return URLName(str_replace(" ","-",$this->name));
    }

    function getLatestThread($user) {
      $db = new db();
      if($this->threadVisibility === 1) {
        $stmt = $db->prepare("SELECT Name,UserId,Id FROM ThreadData WHERE TopicId=? ORDER BY `Date` LIMIT 1");
        $stmt->bind_param("i",$this->id);
      } else {
        $stmt = $db->prepare("SELECT Name,UserId,Id FROM ThreadData WHERE TopicId=? AND (UserId=? OR Pinned=1) ORDER BY `Date` LIMIT 1");
        $stmt->bind_param("ii",$this->id,$user->id);
      }
      $db->exec();
      $result = $db->get();
      $row = $result->fetch_assoc();
      $thread = Thread::withData($id,$row['Name'],$row['UserId'],null,null,null,null,null);
      $thread->id = $row['Id'];
      $thread->exists = true;
      return $thread;
    }

    function countThreads($user) {
      $db = new db();
      if($this->threadVisibility === 1) {
        $stmt = $db->prepare("SELECT COUNT(*) AS Amount FROM ThreadData WHERE TopicId=?");
        $stmt->bind_param("i",$this->id);
      } else {
        $stmt = $db->prepare("SELECT COUNT(*) AS Amount FROM ThreadData WHERE TopicId=? AND (UserId=? OR Pinned=1)");
        $stmt->bind_param("ii",$this->id,$user->id);
      }

      $db->exec();
      $result = $db->get();
      $row = $result->fetch_assoc();
      return (int)$row['Amount'];
    }

    function getThreads($limit, $offset, $user) {
      $db = new db();
      if($this->threadVisibility === 1 || $user->priv >= 2) {
        $stmt = $db->prepare("SELECT Id,Name,UserId,`Date`,LastEdit,Pinned,Locked
          FROM ThreadData WHERE TopicId=?
          ORDER BY Pinned DESC, `Date` DESC
          LIMIT $limit OFFSET $offset");
        $stmt->bind_param("i",$this->id);
      } else {
        $stmt = $db->prepare("SELECT Id,Name,UserId,`Date`,LastEdit,Pinned,Locked
          FROM ThreadData WHERE TopicId=? AND (UserId=? OR Pinned=1)
          ORDER BY Pinned DESC, `Date` DESC
          LIMIT $limit OFFSET $offset");
        $stmt->bind_param("ii",$this->id,$user->id);
      }

      $db->exec();
      $result = $db->get();

      $threads = array();

      if($result->num_rows > 0) {
        $i = 0;
        while($row = $result->fetch_assoc()) {
          $thread = Thread::withData($this->id, $row['Name'], $row['UserId'], $row['Date'], $row['LastEdit'], null, $row['Pinned'], $row['Locked']);
          $thread->id = $row['Id'];
          $thread->exists = true;
          $threads[$i] = $thread;
          $i++;
        }
      }
      return $threads;;
    }
  }

  class Thread {
    var $id;
    var $topicId;
    var $name;
    var $userId;
    var $date;
    var $lastEdit;
    var $content;
    var $pinned;
    var $locked;
    var $exists;

    static function fromId($id, $include_content) {
      $db = new db();
      $stmt = $db->prepare("SELECT * FROM ThreadData WHERE Id=?");
      $stmt->bind_param("i",$id);

      $db->exec();
      $result = $db->get();

      $row = $result->fetch_assoc();

      $instance = new self();

      $instance->id = $id;
      $instance->topicId = $row['TopicId'];
      $instance->name = $row['Name'];
      $instance->userId = $row['UserId'];
      $instance->date = $row['Date'];
      $instance->lastEdit = $row['LastEdit'];
      $instance->pinned = $row['Pinned'];
      $instance->locked = $row['Locked'];
      $instance->exists = true;

      if($include_content) {
        $stmt = $db->prepare("SELECT Text FROM ThreadContent WHERE Id=?");
        $stmt->bind_param("i",$id);

        $db->exec();
        $result = $db->get();

        $row = $result->fetch_assoc();

        $instance->content = $row['Text'];
      }
      return $instance;
    }

    static function withData($topicId, $name, $userId, $date, $lastEdit, $content, $pinned, $locked) {
      $instance = new self();
      $instance->id = getNextId("ThreadData");
      $instance->topicId = $topicId;
      $instance->name = $name;
      $instance->userId = $userId;
      $instance->date = $date;
      $instance->lastEdit = $lastEdit;
      $instance->content = $content;
      $instance->pinned = $pinned;
      $instance->locked = $locked;
      $instance->exists = false;
      return $instance;
    }

    function save() {
      if($this->exists) {
        $db = new db();
        $stmt = $db->prepare("UPDATE ThreadData SET TopicId=?, Name=?, UserId=?, `Date`=?, LastEdit=?, Pinned=?, Locked=? WHERE Id=?;");
        $stmt->bind_param("isisiiii",$this->topicId,$this->name,$this->userId,$this->date,$this->lastEdit,$this->pinned,$this->locked,$this->id);
        $db->exec();
        $stmt = $db->prepare("UPDATE ThreadContent SET `Text`=? WHERE Id=?;");
        $stmt->bind_param("si",$this->content, $this->id);
        $db->exec();
      } else {
        $db = new db();
        $stmt = $db->prepare("INSERT INTO ThreadData VALUES (?,?,?,?,?,?,?,?);");
        $stmt->bind_param("iisisiii",$this->topicId,$this->id,$this->name,$this->userId,$this->date,$this->lastEdit,$this->pinned,$this->locked);
        $db->exec();
        $stmt = $db->prepare("INSERT INTO ThreadContent VALUES (?,?)");
        $stmt->bind_param("is", $this->id, $this->content);
        $db->exec();
        $this->exists = true;
      }
    }

    function getLink($page = -1) {
      if($page === null || $page === -1) {
        return "/thread/$this->id"."-".$this->getName();
      } else {
        return "/thread/$this->id"."-".$this->getName()."/page-".$page;
      }
    }

    function getName() {
      return URLName(str_replace(" ","-",$this->name));
    }

    function getLatestPost() {
      $db = new db();
      $stmt = $db->prepare("SELECT UserId,Id,`Date` FROM Posts WHERE ThreadId=? ORDER BY `Date` DESC LIMIT 1");
      $stmt->bind_param("i",$this->id);
      $db->exec();
      $result = $db->get();
      $row = $result->fetch_assoc();
      $thread = Post::withData($id,$row['UserId'],$row['Date'],null,null);
      $thread->id = $row['Id'];
      $thread->exists = true;
      return $thread;
    }

    function countPosts() {
      $db = new db();
      $stmt = $db->prepare("SELECT COUNT(*) AS Amount FROM Posts WHERE ThreadId=?");
      $stmt->bind_param("i",$this->id);

      $db->exec();
      $result = $db->get();
      $row = $result->fetch_assoc();
      return (int)$row['Amount'];
    }

    function getPosts($limit, $offset) {
      $db = new db();
      $query = "SELECT Id,UserId,`Date`,LastEdit,`Text` FROM Posts WHERE ThreadId=? ORDER BY `Date` LIMIT $limit OFFSET $offset";
      $stmt = $db->prepare($query);
      $stmt->bind_param("i",$this->id);

      $db->exec();
      $result = $db->get();

      $posts = array();

      if($result->num_rows > 0) {
        $i = 0;
        while($row = $result->fetch_assoc()) {
          $post = Post::withData($this->id, $row['UserId'], $row['Date'], $row['LastEdit'], $row['Text']);
          $post->id = $row['Id'];
          $post->exists = true;
          $posts[$i] = $post;
          $i++;
        }
      }
      return $posts;
    }
  }

  class Post {
    var $id;
    var $threadId;
    var $userId;
    var $date;
    var $lastEdit;
    var $content;
    var $exists;

    function __construct() {
      $this->date = "";
      $this->lastEdit = -1;
      $this->content = "";
    }

    static function fromId($id) {
      $db = new db();
      $stmt = $db->prepare("SELECT * FROM Posts WHERE Id=?");
      $stmt->bind_param("i",$id);

      $db->exec();
      $result = $db->get();

      $row = $result->fetch_assoc();

      $instance = new self();

      $instance->id = $id;
      $instance->threadId = $row['ThreadId'];
      $instance->userId = $row['UserId'];
      $instance->date = $row['Date'];
      $instance->lastEdit = $row['LastEdit'];
      $instance->content = $row['Text'];
      $instance->exists = true;

      return $instance;
    }

    static function withData($threadId, $userId, $date, $lastEdit, $content) {
      $instance = new self();
      $instance->id = getNextId("Posts");
      $instance->threadId = $threadId;
      $instance->userId = $userId;
      $instance->date = $date;
      $instance->lastEdit = $lastEdit;
      $instance->content = $content;
      $instance->exists = false;
      return $instance;
    }

    function save() {
      if($this->exists) {
        $db = new db();
        $stmt = $db->prepare("UPDATE Posts SET ThreadId=?, UserId=?, `Date`=?, LastEdit=?, `Text`=? WHERE Id=?;");
        $stmt->bind_param("iisisi",$this->threadId,$this->userId,$this->date,$this->lastEdit,$this->content,$this->id);
        $db->exec();
      } else {
        $db = new db();
        $stmt = $db->prepare("INSERT INTO Posts VALUES (?,?,?,?,?,?);");
        $stmt->bind_param("iiisis",$this->threadId,$this->id,$this->userId,$this->date,$this->lastEdit,$this->content);
        $db->exec();
        $this->exists = true;
      }
    }
  }

  class user {
    var $id;
    var $name;
    var $display;
    var $uuid;
    var $email;
    var $confirmed;
    var $priv;
    var $priv_name;
    var $joined;
    var $lastSeen;
    var $about;
    var $notify;
    var $notifyA = array();

    function reload() {
      $db = new db();
      $stmt = $db->prepare("SELECT Id,Name,Display,Uuid,Email,Confirmed,Priv,Joined,LastSeen,About,Notify FROM Users WHERE Id=?");
      $stmt->bind_param("i",$this->id);
      $db->exec();
      $result = $db->get();

      $data=$result->fetch_assoc();

      $this->name = $data["Name"];
      $this->display = $data["Display"];
      $this->uuid = $data["Uuid"];
      $this->email = $data["Email"];
      $this->confirmed = $data["Confirmed"];
      $this->priv = $data["Priv"];
      $priv_names = array('USER','MOD','ADMIN');
      $this->priv_name = $priv_names[$this->priv - 1];
      $this->joined = $data["Joined"];
      $this->lastSeen = $data["LastSeen"];
      $this->about = $data["About"];
      $this->notify = $data["Notify"];

      // 0 = no notifications
      // 1 = notification on website
      // 2 = notification by email
      // 3 = notification by both

      $this->notifyA[0] = ($this->notify & 1 ? (($this->notify & 2) ? 3 : 1) : ($this->notify & 2 ? 2 : 0));
      $this->notifyA[1] = ($this->notify & 4 ? (($this->notify & 8) ? 3 : 1) : ($this->notify & 8 ? 2 : 0));
      $this->notifyA[2] = ($this->notify & 16 ? (($this->notify & 32) ? 3 : 1) : ($this->notify & 32 ? 2 : 0));
    }

    function __construct($id) {
      if($id < 0) {
        $this->id = $id;
        $this->priv = 1;
      } else {
        $this->id = $id;
        $this->reload();
      }
    }

    function update() {
      $db = new db();
      $stmt = $db->prepare("UPDATE Users SET Display=?, Email=?, Confirmed=?, Priv=?, LastSeen=?, About=?, Notify=? WHERE Id=?");
      $stmt->bind_param("ssisssii",$this->display,$this->email,$this->confirmed,$this->priv,$this->lastSeen,$this->about,$this->notify,$this->id);
      $db->exec();
    }

    function getImage($size) {
      return "https://crafatar.com/avatars/".$this->uuid."?size=".$size;
    }

    function hasSeen($threadId) {
      $db = new db();
      $stmt = $db->prepare("SELECT COUNT(*) AS Amount FROM Seen WHERE UserId=? AND ThreadId=?");
      $stmt->bind_param("ii", $this->id, $threadId);
      $db->exec();
      $result = $db->get();
      if($result->fetch_assoc()["Amount"] > 0) {
        return 1;
      }
      return 0;
    }

    function getLink() {
      return "/account/".$this->id."-".$this->getName();
    }

    function getName() {
      return URLName(str_replace("%20","-",urlencode($this->name)));
    }
  }

  class db {

    var $conn;
    var $stmt;

    function prepare($query) {
      $server = "localhost";
      $user = $GLOBALS['sql_user'];
      $pass = $GLOBALS['sql_pass'];
      $db = "zerentha_forums";

      $this->conn = new mysqli($server, $user, $pass, $db);

      if ($this->conn->connect_error) {
        die("Connection failed: " . $this->conn->connect_error);
      }

      $this->stmt = $this->conn->prepare($query);
      return $this->stmt;
    }

    function exec() {
      $this->stmt->execute();
    }

    function get() {
      return $this->stmt->get_result();
    }

    function close() {
      mysqli_close($this->conn);
    }

  }


  class db_minecraft {

    var $conn;
    var $stmt;

    function prepare($query) {
      $server = "localhost";
      $user = $GLOBALS['sql_user'];
      $pass = $GLOBALS['sql_pass'];
      $db = "zerentha_minecraft";

      $this->conn = new mysqli($server, $user, $pass, $db);

      if ($this->conn->connect_error) {
        die("Connection failed: " . $this->conn->connect_error);
      }

      $this->stmt = $this->conn->prepare($query);
      return $this->stmt;
    }

    function exec() {
      $this->stmt->execute();
    }

    function get() {
      return $this->stmt->get_result();
    }

    function close() {
      mysqli_close($this->conn);
    }

  }

 ?>
