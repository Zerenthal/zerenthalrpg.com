<?php
  session_start();
  include_once('api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<?php
imports();
 ?>

<?php
$GLOBALS['display_changed'] = true;
if($_GET['id'] === null) {
  $GLOBALS['user'] = getUser();
} elseif(userExists($_GET['id'])){
  $GLOBALS['user'] = new user($_GET['id']);
} else {
  echo "<script>
    window.location.href = \"/404\";
  </script>";
}

if ( $_SERVER['REQUEST_METHOD'] == 'POST' && isLoggedIn()) {
  if(getUser()->priv < 2) {
    $GLOBALS['user'] = getUser();
  }
  $user->about = $_POST['about'];

  if(getUser()->priv >= 3) {
    $user->priv = $_POST['rank'];
  }

  $db = new db();
  $stmt = $db->prepare("SELECT Display FROM Users WHERE Display=?;");
  $display = htmlspecialchars($_POST['display']);
  $stmt->bind_param("s",$display);
  $db->exec();
  $result = $db->get();

  if($result->num_rows > 0 && $_POST['display'] !== $user->display) {
    $GLOBALS['display_changed'] = false;
  } else {
    $user->display = htmlspecialchars($_POST['display']);
  }
  $user->update();
  echo "<script>
    window.location.href = \"".$user->getLink()."\";
  </script>";
}
?>

<title><?php echo $user->display ?> ZerenthalRPG</title>

</head>

<body onload="onload();">

  <?php print_header(-1); ?>

  <div class="main" id="main">

    <div class="body">

      <?php if ($user->display === null) { ?>

        <h1>User does not exist!</h1>
      <?php } else { ?>

        <h1><?php echo $user->display; ?></h1>

        <?php
        if($_GET['id'] === null || (int)$_GET['id'] === (int)$_SESSION['id'] || getUser()->priv > 1) {
          echo "<div class=\"edit_account_button right\">
            <a href=\"".$user->getLink()."/edit\" class=\"edit_account_button\">Edit</a>
          </div>";
        }
        ?>
        <img class="user-avatar" src="<?php echo $user->getImage(150);?>" alt="Player Head">
        <table class="info-table">
          <tr>
            <td>
              Minecraft Name
            </td>
            <td style="font-weight: normal;">
              <?php echo $user->name; ?>
            </td>
          </tr>
          <tr>
            <td>
              Rank
            </td>
            <td style="font-weight: normal;">
              <div class="rank <?php echo $user->priv_name; ?>_RANK"><?php echo $user->priv_name; ?></div>
            </td>
          </tr>
          <tr>
            <td>
              UUID
            </td>
            <td style="font-weight: normal;">
              <?php echo $user->uuid; ?>
            </td>
          </tr>
          <tr>
            <td>
              Joined
            </td>
            <td style="font-weight: normal;">
              <?php print_time($user->joined); ?>
            </td>
          </tr>
        </table>
        <div class="about">
          <strong>About</strong><br>
          <?php
          $text = $user->about;
          if(strlen($text) === 0) {
            $text = "*Nothing*";
          }
          echo markdown($text);
          ?>
        </div>
      <?php } ?>
    </div>

  </div>

</body>

</html>
