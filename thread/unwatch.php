<?php
session_start();
include_once('../api.php');

$user = getUser();
if($user->id !== -1 && isset($_GET['id'])) {
  $db = new db();
  $stmt = $db->prepare("DELETE FROM Watched WHERE ThreadId=? AND UserId=?");
  $stmt->bind_param("ii",$_GET['id'],$user->id);
  $db->exec();
}
?>
