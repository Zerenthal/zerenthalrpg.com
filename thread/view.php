<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<?php
function removeQuotes($text) {
  return htmlspecialchars(str_replace('\'','\\\'',$text));
}

$id = $_GET['id'];
$GLOBALS['thread'] = Thread::fromId($id, true);
metaInfo($thread->name, $thread->getLink(),getUser($thread->userId)->getImage(100).".png");
imports();
if($id === null) {
	echo "<script> window.location.href = \"/forums\"; </script>";
}
$GLOBALS['page'] = $_GET['page'];
if($page === null) {
	$GLOBALS['page'] = 1;
}
$GLOBALS['topic'] = Topic::fromId($thread->topicId);
$GLOBALS['pageAmount'] = 10;

$GLOBALS['user'] = getUser();

if(!($user->hasSeen($id)) && $user->id != -1) {
  $db = new db();
  $stmt = $db->prepare("INSERT INTO Seen VALUES (?,?,?)");
  $stmt->bind_param("iii",$nextId,$id,$user->id);
  $nextId = getNextId("Seen");
  $db->exec();
}
 ?>

<title><?php echo $thread->name; ?></title>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">
      <?php
      if($topic->visible<=$user->priv) {
        $poster = getUserFromId($thread->userId);

        $posts = $thread->getPosts($pageAmount, $pageAmount * ($page - 1));

        echo "<span><a href=\"/forums\">Forums</a> → <a href=\"".$topic->getLink()."\">$topic->name</a> → $thread->name</span>
        <h1>$thread->name</h1>";

        $db = new db();
        $stmt = $db->prepare("SELECT Count(*) AS Amount FROM Watched WHERE UserId=? AND ThreadId=?");
        $stmt->bind_param("ii",$user->id,$thread->id);
        $db->exec();
        $result = $db->get();
        $row = $result->fetch_assoc();
        echo "<div class=\"btn\" id=\"watch_btn\" data-thread=\"$thread->id\">";
        if($row['Amount'] > 0) {
          echo "Unwatch</div>";
        } else {
          echo "Watch</div>";
        }

        echo "<div class=\"group with-img\">
          <div class=\"group-header\">
            <a href=\"".$poster->getLink()."\"><img src=\"".$poster->getImage(64)."\" class=\"side-img\"></a>
            <div class=\"rank\">
              <a href=\"".$poster->getLink()."\" class=\"".$poster->priv_name."_RANK\">$poster->display</a>
            </div> ";
        print_time($thread->date);
        if(($thread->userId === $userId || $user->priv >= 2) && ($thread->locked === 0 || $user->priv>=2)) {
          echo "<div class=\"right\" style=\"margin-left: 10px;\"><a href=\"/thread/edit-".$_GET['id']."-".$thread->getName()."\">Edit</a></div>";
        }
        echo "<a><span class=\"right\" onclick=\"replyTo('".removeQuotes($poster->name)."','".trim(preg_replace('/\s+/', ' ', removeQuotes($thread->content)))."');\"> Reply</span></a>";
        if($thread->lastEdit > 0) {
          $editor = getUserFromId($thread->lastEdit);
          echo "<span class=\"right\">
                Last edited by
                <div class=\"rank\">
                  <a href=\"".$editor->getLink()."\" class=\"".$editor->priv_name."_RANK\">$editor->display</a>
                </div>
              </span>";
        }
        echo "<div class=\"clear-right\"></div>
        </div>
        <div class=\"group-content\">
            ".markdown($thread->content)."
          </div>
        </div>";

        $postAmount = $thread->countPosts();
        echo "<div class=\"replies\" id=\"replies\"><strong>$postAmount ";
        if($postAmount === 1) {
          echo "reply";
        } else {
          echo "replies";
        }
        echo "</strong></div>";

        foreach ($posts as &$post) {
          $poster = getUserFromId($post->userId);
          $posterLink = "href=\"".$poster->getLink()."\"";

          echo "<div class=\"group with-img\" id=\"post$post->id\">
            <div class=\"group-header\">
              <a $posterLink><img src=\"".$poster->getImage(64)."\" class=\"side-img\"></a>
              <div class=\"rank\">
                <a $posterLink class=\"".$poster->priv_name."_RANK\">$poster->display</a>
              </div> ";
          print_time($post->date);
          if(($post->userId === $userId || $user->priv >= 2) && ($thread->locked === 0 || $user->priv>=2)) {
            echo "<div class=\"right\" style=\"margin-left: 10px;\"><a href=\"/thread/reply-".$post->id."\">Edit</a></div>";
          }
          echo "<a><span class=\"right\" onclick=\"replyTo('".removeQuotes($poster->name)."','".trim(preg_replace('/\s+/', ' ', removeQuotes($post->content)))."');\"> Reply</span></a>";
          if($post->lastEdit > 0) {
            $editor = getUserFromId($post->lastEdit);
            echo "<span class=\"right\">
                  Last edited by
                  <div class=\"rank\">
                    <a href=\"".$editor->getLink()."\" class=\"".$editor->priv_name."_RANK\">$editor->display</a>
                  </div>
                </span>";
          }
          echo "<div class=\"clear-right\"></div>
          </div>
          <div class=\"group-content\">
              ".markdown($post->content)."
            </div>
          </div>";
        }
        print_pages($page, (int)ceil($postAmount / $pageAmount), $thread->getLink());
        if($thread->locked) {
          echo "<div class=\"warning\">This thread is locked</div>";
        } elseif ($user->priv < $topic->visible) {
          echo "<div class=\"warning\">You do not have permission to post here.</div>";
        } elseif ($user->id === -1) {
          echo "Please <a href=\"/login\">Log in</a> to post a reply.";
        } else {
          echo "<form method=\"POST\" action=\"/thread/post_reply.php?id=".$_GET['id']."&page=$page\">";
          print_editor("replyText","Say what you'd like!","");
          echo "</form>";
        }
      } else {
        echo "<h1>I'm sorry, you do not have permission to view this topic.</h1>";
      }
      ?>

    </div>

  </div>

</body>

</html>
