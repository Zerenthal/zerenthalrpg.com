<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>Editing Thread</title>

<?php
imports();
$GLOBALS['user'] = getUser();
if($_GET['id'] === null) {
  echo "<script> window.location.href = \"../forums\"; </script>";
} else {
  $GLOBALS['thread'] = Thread::fromId($_GET['id'],true);
  $GLOBALS['topic'] = Topic::fromId($thread->topicId);
}

if(isset($_POST['name']) && ($thread->userId === $user->id || $user->priv >= 2) && ($thread->locked === 0 || $user->priv >= 2)) {
  $thread->content = $_POST['content'];
  $thread->name = htmlspecialchars($_POST['name'], ENT_QUOTES);
  $thread->locked = ($_POST['locked'] === "1");
  $thread->pinned = ($_POST['pinned'] === "1");
  $thread->lastEdit = $user->id;
  $thread->save();
  $post = Post::withData($thread->id,$user->id,null,null,null);
  $post->id = -1;
  notifyUsers($thread,$post,2);
  echo "<script> window.location.href = \"".$thread->getLink()."\"; </script>";
}
?>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">

      <?php
      echo "<span><a href=\"/forums\">Forums</a> → <a href=\"".$topic->getLink()."\">$topic->name</a> → <a href=\"".$thread->getLink()."\">$thread->name</a> → Edit Thread</span>
        <h1>Editing Thread</h1>";
      if(($thread->userId === $user->id || $user->priv >= 2) && ($thread->locked === 0 || $user->priv >= 2)) {
        echo "<a class=\"btn btn_warn\" href=\"delete-confirm-".$_GET['id']."\">Delete</a>
        <form method=\"POST\" action=\"edit.php?id=".$_GET['id']."\">
          <input type=\"text\" name=\"name\" placeholder=\"Title\" value=\"".$thread->name."\">";
          if($user->priv >= 2) {
            echo "<label for=\"pinned\">Pin</label><input id=\"pinned\" type=\"checkbox\" name=\"pinned\" value=\"1\" ".($thread->pinned ? "checked" : "").">
            <label for=\"locked\">Lock</label><input id=\"locked\" type=\"checkbox\" name=\"locked\" value=\"1\" ".($thread->locked ? "checked" : "").">";
          }
          print_editor("content", "Say what you'd like!", $thread->content);
        echo "</form>";
      } else {
        echo "<h1>You cannot edit this post!</h1>";
      }
       ?>

    </div>

  </div>

</body>

</html>
