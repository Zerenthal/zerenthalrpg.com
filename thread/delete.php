<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>Delete Thread</title>

<?php
imports();

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['id'])) {
  $user = getUser();
  $thread = Thread::fromId($_GET['id'],false);
  if($thread->userId === $user->id || $user->priv >= 2) {
    deleteThreads("Id=".$_GET['id']);
  }
  ?>

 <script>
   <?php
   $topic = Topic::fromId($thread->topicId);
    ?>
   window.location.href = "<?php echo $topic->getLink(); ?>";
 </script>

  <?php
}
 ?>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">
      <h1 style="text-align: center;">Are you sure you want to delete this thread?</h1>
      <form method="POST" action="delete-confirm-<?php echo $_GET['id']; ?>">
        <table>
          <tr>
            <td>
              <input type="submit" value="Delete" style="background-color: #f33;">
            </td>
            <td>
              <?php $thread = Thread::fromId($_GET['id'], false); ?>
              <a href="<?php echo $thread->getLink(); ?>"><input type="button" value="Cancel"></a>
            </td>
          </tr>
        </table>
      </form>
    </div>

  </div>

</body>

</html>
