<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>Delete Thread</title>

<?php
imports();

if(isset($_GET['id'])) {
  $user = getUser();
  $post = Post::fromId($_GET['id']);
  $thread = Thread::fromId($post->threadId,false);
  $db = new db();
  if(($post->userId === $user->id || $user->priv >= 2) && $thread->locked === 0) {
    $stmt = $db->prepare("DELETE FROM Posts WHERE Id=?");
    $stmt->bind_param("i",$post->id);
    $db->exec();
    $stmt = $db->prepare("DELETE FROM Notifications WHERE PostId=?");
    $stmt->bind_param("i",$post->id);
    $db->exec();
  }
}
 ?>

<script>
  window.location.href = "<?php echo $thread->getLink(); ?>";
</script>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">

    </div>

  </div>

</body>

</html>
