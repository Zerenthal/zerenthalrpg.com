<?php
  session_start();
  include_once('../api.php');

  $user = getUser();
  $thread = Thread::fromId($_GET['id'],false);
  $topic = Topic::fromId($thread->topicId);

  $canPost = ($user->priv >= $topic->visible && $thread->locked === 0 && $user->id !== -1);

  if(isset($_POST['replyText']) && $_POST['replyText'] !== "" && $canPost) {
    $post = Post::withData($thread->id, $user->id, gmdate(DATE_ATOM), -1, $_POST['replyText']);
    $post->save();
    $notified = notifyUsers($thread,$post,0);
    notifyMentioned($thread,$post,$notified);
  }
?>
<head>
  <script>
    window.location.href = "<?php echo $thread->getLink($_GET['page']); ?>";
  </script>
</head>
