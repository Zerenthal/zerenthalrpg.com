<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>New Thread</title>

<?php
imports();
$GLOBALS['user'] = getUser();

$GLOBALS['topic'] = Topic::fromId($_GET['id']);
if($_GET['id'] === null) {
  echo "<script> window.location.href = \"../forums\"; </script>";
}

if(isset($_POST['name']) && $user->id !== -1) {
  $thread = Thread::withData($topic->id, htmlspecialchars($_POST['name'], ENT_QUOTES), $user->id, gmdate(DATE_ATOM), -1, $_POST['content'], 0, 0);
  $thread->save();
  $db = new db();
  $stmt = $db->prepare("INSERT INTO Watched VALUES (?,?,?)");
  $stmt->bind_param("iii",$id,$user->id,$thread->id);
  $id = getNextId("Watched");
  $db->exec();
  $post = Post::withData($thread->id, $user->id, "", -1, $thread->content);
  $post->id = -1;
  notifyMentioned($thread,$post,Array());
  echo "<script> window.location.href = \"".$thread->getLink()."\"; </script>";
}
?>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">

      <?php
      echo "<span><a href=\"../forums\">Forums</a> → <a href=\"".$topic->getLink()."\">$topic->name</a> → Create Thread</span>
      <h1>New Thread</h1>";
      if($user->priv >= $topic->canPost) {
        if($user->id !== -1) {
          echo "<form method=\"POST\" action=\"/thread/create.php?id=".$_GET['id']."\">
            <input type=\"text\" name=\"name\" placeholder=\"Title\">";
          print_editor("content", "Say what you'd like!", "");
          echo "</form>";
        } else {
          echo "Please <a href=\"../login\">Log in</a> to post a thread.";
        }
      } else {
        echo "<h1>You cannot post here!</h1>";
      }
       ?>

    </div>

  </div>

</body>

</html>
