<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>Editing Reply</title>

<?php
imports();
$GLOBALS['user'] = getUser();

$GLOBALS['post'] = Post::fromId($_GET['id']);
$GLOBALS['thread'] = Thread::fromId($post->threadId,false);
$GLOBALS['topic'] = Topic::fromId($thread->topicId);

$canPost = ($user->id === $post->userId || $user->priv >= 2) && ($thread->locked === 0 || $user->priv >= 2);

if(isset($_POST['replyText']) && $canPost) {
  $post->lastEdit = $user->id;
  $post->content = $_POST['replyText'];
  $post->save();
  echo "<script> window.location.href=\"".$thread->getLink()."\" </script>";
}
?>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">

      <?php
      echo "<span><a href=\"../forums\">Forums</a> → <a href=\"".$topic->getLink()."\">$topic->name</a> → <a href=\"".$thread->getLink()."\">$thread->name</a> → Edit Reply</span>
      <h1>Editing Reply</h1>";
      if(($post->userId === $user->id || $user->priv >= 2) && ($thread->locked === 0 || $user->priv >= 2)) {
        echo "<a class=\"btn btn_warn\" href=\"delete_reply.php?id=".$_GET['id']."\">Delete</a>
        <form method=\"POST\" action=\"edit_reply.php?id=".$_GET['id']."\" style=\"margin-top: 10px;\">";
        print_editor("replyText", "Say what you'd like!", $post->content);
        echo "</form>";
      } else {
        echo "<div class=\"warning\">You cannot edit this post!";
      }
       ?>

    </div>

  </div>

</body>

</html>
