<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>Create Category</title>

<?php
imports();
$GLOBALS['user'] = getUser();

if($user->priv >= 3 && isset($_POST['name'])) {
  $category = Category::withData($_POST['name'],$_POST['visible']);
  $category->save();
  echo "<script> window.location.href = \"/forums\"; </script>";
}
 ?>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">

      <?php
      echo "<span><a href=\"../forums\">Forums</a> → Create Category</span>
      <h1>Creating Category</h1>";
      if($user->priv >= 3) {
        echo "<form method=\"POST\" action=\"create.php\">
          <table>
            <tbody>
              <tr>
                <td>
                  Name
                </td>
                <td>
                  <input type=\"text\" name=\"name\" placeholder=\"category name\">
                </td>
              </tr>
              <tr>
                <td>
                  Visible to
                </td>
                <td>
                  <select name=\"visible\">
                    <option value=\"1\">Everyone</option>
                    <option value=\"2\">Mods and admins</option>
                    <option value=\"3\">Only admins</option>
                  </select>
                </td>
              </tr>
            </tbody>
          </table>
          <input type=\"submit\" value=\"Create\">
        </form>";
      } else {
        echo "<h1>You do not have permission to create a category!</h1>";
      }
      ?>
    </div>

  </div>

</body>

</html>
