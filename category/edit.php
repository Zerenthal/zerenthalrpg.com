<?php
  session_start();
  include_once('../api.php');
?>
<!DOCTYPE HTML>

<html>

<head>

<title>Editing Category</title>

<?php
imports();
$GLOBALS['user'] = getUser();
$GLOBALS['category'] = Category::fromId($_GET['id']);

if($user->priv >= 3 && isset($_POST['name'])) {
  $category->name = $_POST['name'];
  $category->visible = $POST['visible'];
  $category->save();
  echo "<script> window.location.href = \"/forums\"; </script>";
}
 ?>

</head>

<body onload="onload();">

  <?php print_header(1); ?>

  <div class="main" id="main">

    <div class="body">

      <?php
      if($user->priv >= 3) {
        echo "<span><a href=\"/forums\">Forums</a> → Edit Category</span>";
        if($category->name === null) {
          echo "<h1>Category could not be found!</h1>";
        } else {
          echo "<h1>Editing $category->name</h1>";
          echo "<form method=\"POST\" action=\"edit.php?id=$category->id\">
            <table>
              <tbody>
                <tr>
                  <td>
                    Name
                  </td>
                  <td>
                    <input type=\"text\" name=\"name\" placeholder=\"category name\" value=\"$category->name\">
                  </td>
                </tr>
                <tr>
                  <td>
                    Visible to
                  </td>
                  <td>
                    <select name=\"visible\">
                      <option value=\"1\" ".($category->visible === 1 ? "selected=\"selected\"" : "").">Everyone</option>
                      <option value=\"2\" ".($category->visible === 2 ? "selected=\"selected\"" : "").">Mods and admins</option>
                      <option value=\"3\" ".($category->visible === 3 ? "selected=\"selected\"" : "").">Only admins</option>
                    </select>
                  </td>
                </tr>
              </tbody>
            </table>
            <input type=\"submit\" value=\"Edit\">
          </form>";
        }
      } else {
        echo "<h1>You do not have permission to edit a category!</h1>";
      }
      ?>

    </div>

  </div>

</body>

</html>
