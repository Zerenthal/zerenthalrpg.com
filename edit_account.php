<?php
  session_start();
  include_once('api.php');

  $GLOBALS['sessionUser'] = getUser();

  if($_GET['id'] === null) {
    $GLOBALS['user'] = getUser();
  } else if(userExists($_GET['id'])){
    $GLOBALS['user'] = new user($_GET['id']);
  } else {
    echo "<script>
      window.location.href = \"../404.shtml\";
    </script>";
  }
?>
<!DOCTYPE HTML>

<html>

<head>

<title>Editing <?php echo $user->display; ?></title>

<?php
imports();
 ?>

</head>

<body onload="onload();">

  <?php print_header(-1); ?>

  <div class="main" id="main">

    <div class="body">

    	<h1>Editing <?php echo $user->display; ?></h1>

    	<form method="POST" action="/account.php?id=<?php echo $user->id; ?>">

    		<table>
    			<tr>
    				<td>
    					Display Name
    				</td>
    				<td>
    					<input id="text_display_name" type="text" name="display" value="<?php echo $user->display; ?>">
    				</td>
    			</tr>
          <tr>
            <?php if($sessionUser->priv >= 3) { ?>
            <td>
              Rank
            </td>
            <td>
              <select name="rank">
                <option value="1" <?php echo ($user->priv === 1 ? "selected" : ""); ?>>User</option>
                <option value="2" <?php echo ($user->priv === 2 ? "selected" : ""); ?>>Mod</option>
                <option value="3" <?php echo ($user->priv === 3 ? "selected" : ""); ?>>Admin</option>
              </select>
            </td>
            <?php } ?>
          </tr>
    			<tr>
	    			<td>
	    				About
	    			</td>
	    			<td>
	    				<?php print_editor("about","What do you want everyone to know about you?", $user->about, "account.php") ?>
	    			</td>
    			</tr>
    		</table>
    	</form>
    </div>

   </div>

</body>

</html>
